import React, { Component } from 'react';
import {connect} from "react-redux";
import {Link} from 'next/link';
import {getPasswordResetCheck} from "../../../actions/Users/resetpassword/check";
import {savePasswordReset} from "../../../actions/Users/resetpassword/save";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class createPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      password2: "",
      passwordError: "",
      passwordSaveError: false,
      passwordSaveSuccess: false,
      submitting: false
    };
  }

  handleChange(e) {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({[name]: value});
  }

  setPassword() {
    this.setState({passwordError: ""});
    const {password, password2} = this.state;
    if (password.length < 1) {
      this.setState({passwordError: "Please enter a password."});
      return;
    }
    if (password2.length < 1) {
      this.setState({passwordError: "Please confirm your password."});
      return;
    }
    if (password.length < 6) {
      this.setState({passwordError: "Your password must be at least 6 characters long"});
      return;
    }
    if (password !== password2) {
      this.setState({passwordError: "Your passwords do not match."});
      return;
    }
    if (password !== password2) {
      this.setState({passwordError: "Your passwords do not match."});
      return;
    }
    this.setState({submitting: true});
    this.props.dispatch(savePasswordReset({password, password2, code: this.props.query.createCode}));
  }

  componentDidMount() {
    if(this.props.query && this.props.query.createCode) {
      this.setState({createing: true});
      this.props.dispatch(getPasswordResetCheck(this.props.query.createCode));
    } else {
      this.setState({creationError: "No password creation code present"})
    }
  }

  componentDidUpdate(prevProps) {
    const saveCheck = this.props.ResetPasswordSave;
    const prevSaveCheck = prevProps.ResetPasswordSave;
    if (prevSaveCheck.loading && !saveCheck.loading && !this.state.passwordSaveSuccess) {
      if (saveCheck.error && !this.state.passwordSaveError) {
        this.setState({passwordError: "There was an error saving your password: " + saveCheck.error, passwordSaveError: true});
        return;
      }
      if (saveCheck.result) {
        this.setState({passwordSaveSuccess: true});
      }
    }
  }

  render() {
    const initCheck = this.props.ResetPasswordCheck;
    const saveCheck = this.props.ResetPasswordSave;
    const {creationError, password, password2, submitting, passwordSaveError, passwordSaveSuccess} = this.state;
    if (passwordSaveSuccess) {
      return (<div className="password-save success">Your password has been successfully saved. You can now <a href="/users/login">Log In</a> </div>);
    }
    if (creationError) return (<div>{creationError}</div>);
    if (initCheck.loading) return (<div>Confirming creation code...</div>);
    if (!initCheck.loading && initCheck.error) return (<div>{initCheck.error}</div>);
    if (!initCheck.loading && initCheck.result) {
      let passwordError = "";
      if (this.state.passwordError && this.state.passwordError.length) {
        passwordError = (
          <div className='create-password-error'>
            {this.state.passwordError}
          </div>
        );
      }
      if (initCheck.result.valid) {
        return (
          <div className='create-password-holder'>
            <h3>Create Password</h3>
            <div className='create-password-message'>
              Create your password below to start using Bespeek. Please ensure that your password is secure and ONLY YOU know it.
            </div>
            <div className='create-password-form'>
              {passwordError}
              <Form>
                <FormGroup>
                  <Label for="firstPassword">Password</Label>
                  <Input type="password" name="password" id="firstPassword" value={password} onChange={this.handleChange.bind(this)} disabled={submitting} placeholder="Super secret password" />
                </FormGroup>
                <FormGroup>
                  <Label for="secondPassword">Repeat Password</Label>
                  <Input type="password" name="password2" id="secondPassword" value={password2} onChange={this.handleChange.bind(this)} disabled={submitting} placeholder="Repeat your super secret password" />
                </FormGroup>
                <Button outline color="success" className='password-submit' disabled={submitting} onClick={this.setPassword.bind(this)}>{submitting ? "Creating..." : "Submit"}</Button>
              </Form>
            </div>
          </div>
        );
      } else {
        return (<div>A user account with that creation code could not be found.</div>);
      }
    }
  }
}

const mapStateToProps = (state) => {
	return {
    ResetPasswordCheck: {
      result: state.ResetPasswordCheck.result,
      loading: state.ResetPasswordCheck.loading,
      error: state.ResetPasswordCheck.error
    },
    ResetPasswordSave: {
      result: state.ResetPasswordSave.result,
      loading: state.ResetPasswordSave.loading,
      error: state.ResetPasswordSave.error
    }
	};
}

export default connect(mapStateToProps)(createPassword);