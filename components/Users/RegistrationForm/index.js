import React, { Component } from 'react';
import {connect} from "react-redux";
import {registerUser} from "../../../actions/Users/register";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import validateRegistration from "../../../validation/Users/register";
import './style.scss';

class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      first_name: "",
      last_name: "",
      zip_code: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({[name]: value});
  }

  registerUserDispatch(email, password, first_name, last_name) {
    const invalid = validateRegistration.validate({email, password, first_name, last_name});
    // If `invalid` is anything but null, we received an error to display
    if (invalid) {
      this.setState({"formError": invalid});
      return;
    }
    this.props.dispatch(registerUser({email, password, first_name, last_name}));
  }

  render() {
    const {email, password, first_name, last_name, zip_code, formError} = this.state;
    const {result, loading, error} = this.props;
    let form = [(
      <div  className="reg-wrap" key="reg-form-title"><h3 className="form-title">Register For Bespeek</h3></div>
    )];
    if (loading) {
      form.push((
        <div key="reg-form-loading">Registering...</div>
      ));
    } else if (!loading && !error && result.message) {
      form.push((
        <div>
          <div key="reg-form-result">{result.message}</div>
          <div key="reg-form-check-your-email">We've sent a confirmation email to the address you provided to verify your account.</div>
        </div>
      ));
    } else {
      if ((error || formError) && !loading) {
        form.push((
          <div key="reg-form-error" className={"alert alert-danger"}>
            There was an error with your registration: {error || formError}
          </div>
        ));
      }
      form.push((
        <div key="reg-form">
          <Form className="user_registration">
          <FormGroup>
            <Label for="emailAddress">Email</Label>
            <Input type="email" name="email" id="emailAddress" placeholder="you@youremail.com" value={email} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password" name="password" id="password" placeholder="Your password" value={password} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup>
            <Label for="firstName">First Name</Label>
            <Input type="text" name="first_name" id="firstName" placeholder="First" value={first_name} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup>
            <Label for="lastName">Last Name</Label>
            <Input type="text" name="last_name" id="lastName" placeholder="Last" value={last_name} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup>
            <Label for="zipCode">Zip Code</Label>
            <Input type="text" name="zip_code" id="zipCode" placeholder="zip" value={zip_code} onChange={this.handleChange} />
          </FormGroup>
            <Button color="success" onClick={() => this.registerUserDispatch(email, password, first_name, last_name)}>Submit</Button>
          </Form>
        </div>
      ));
    }
    return form;
  }
}

const mapStateToProps = (state) => {
	return {
	  result: state.UserRegistration.result,
	  loading: state.UserRegistration.loading,
	  error: state.UserRegistration.error
	};
}

export default connect(mapStateToProps)(RegistrationForm);