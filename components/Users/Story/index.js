import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getUserStory} from "../../../../actions/Users/getStory";
import config from '../../../../env';
import './style.scss';

class UserStory extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount(){
    this.props.dispatch(getUserStory());
  }
  render(){

    const {result, error, loading} = this.props;
    if (loading || !result || !result.story || !result.story.categories) return (<div></div>);
    if (error) return(<div>There was an error loading the User Story: {error}</div>);
    const story = result.story;
    const categories = story.categories;
    const sponsor = story.sponsor;

    const categoriesArray = [];
    categories.forEach(cat => {
      const questionsArray = [];
      cat.questions.forEach(catq => {
        questionsArray.push(<div>{catq.question + ": " + catq.answer}</div>);
      });
      categoriesArray.push(<div>{cat.category_name + ": " + cat.category_description} {questionsArray}</div>);
    });

    return (
      <div>
        <div className="user-arrangements">
          <div className="row">
            <div className="col-lg-10 col-md-9 col-sm-8 ">
              <img className="user-profile-pic img-fluid" src={story.sponsor.logo} />
            </div>
            <div className="col-lg-2 col-md-3 col-sm-4">
              <img className="user-profile-pic img-fluid" src={story.user.profile_image} />
            </div>
          </div>
        </div>
        <div>{categoriesArray}</div>
        <div>{story.user.first_name} {story.user.last_name}</div>
      </div>
      )
  }

}

const mapStateToProps = (state) => {
    return {
      result: state.UserStory.result,
      loading: state.UserStory.loading,
      error: state.UserStory.error
    };
}

export default connect(mapStateToProps)(UserStory);