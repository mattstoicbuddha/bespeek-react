import React, { Component } from 'react';
import {connect} from "react-redux";
import Router from 'next/router';
import {loginUser, loginUserClearState} from "../../../actions/Users/login";
import {getPasswordReset} from "../../../actions/Users/resetpassword/get";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import validateLogin from "../../../validation/Users/login";
import cookieHelper from '../../../helpers/cookies';
import './style.scss';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };

    const token = cookieHelper.getTokenCookie();
    if (token) {
      this.state.token = token;
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleKeypress(event) {
    if (event.which === 13) {
      // Enter key was pressed
      this.loginUserDispatch(this.state.email, this.state.password);
    }
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({[name]: value});
  }

  loginUserDispatch(email, password) {
    const invalid = validateLogin.validate({email});
    // If `invalid` is anything but null, we received an error to display
    if (invalid) {
      this.setState({"formError": invalid});
      return;
    }
    this.props.dispatch(loginUser({email, password}));
  }

  componentDidMount(){
    let token = cookieHelper.getTokenCookie();
    let role = cookieHelper.getRole();
    if (!token && this.props.UserLogin && this.props.UserLogin.token) {
      if (this.props.UserLogin.sponsor_info) {
        cookieHelper.setSponsor();
      }
      token = this.props.UserLogin.token;
      role = this.props.UserLogin.role;
      // Clear our local bespeek token from state/props
      this.props.dispatch(loginUserClearState());
    }
    if (token) {
      // Set up our token and get us to the profile
      cookieHelper.setTokenCookie(token, role);
      if (cookieHelper.isSponsor()) {
        Router.push("/sponsors/dashboard");
        return;
      }
      Router.push("/users/profile");
    }
  }

  componentDidUpdate(prevProps){
    let token = cookieHelper.getTokenCookie();
    let role = cookieHelper.getRole();
    if (!token && this.props.UserLogin && this.props.UserLogin.token) {
      if (this.props.UserLogin.sponsor_info) {
        cookieHelper.setSponsor();
      }
      role = this.props.UserLogin.role;
      token = this.props.UserLogin.token;
      // Clear our local bespeek token from state/props
      this.props.dispatch(loginUserClearState());
    }
    if (token) {
      // Set up our token and get us to the profile
      cookieHelper.setTokenCookie(token, role);
      if (cookieHelper.isSponsor()) {
        Router.push("/sponsors/dashboard");
        return;
      }
      Router.push("/users/profile");
    }
    if (prevProps.ResetPasswordGet.loading && !this.props.ResetPasswordGet.loading) {
      if (this.props.ResetPasswordGet.result.updated) {
        alert("Password reset sent. Check your email!");
      } else if (this.props.ResetPasswordGet.error) {
        alert(this.props.ResetPasswordGet.error)
      }
    }
  }

  resetPassword(email){
    this.props.dispatch(getPasswordReset(email));
  }

  render() {
    const {email, password, formError} = this.state;
    const {result, loading, error, token} = this.props.UserLogin;
    const resetPassGet = this.props.ResetPasswordGet;
    const user_token = cookieHelper.getTokenCookie() || token;
    if (user_token) {
      console.log("Yep, no render here...");
      // We've already logged in, nothing to render
      return (<div></div>);
    }

    let form = [(
      <div key="reg-form-title"><h3 className="form-title">Login</h3></div>
    )];

    if (loading) {
      form.push((
        <div key="login-form-loading">Logging you in...</div>
      ));
    } else if (!loading && !error && result.token) {
      form.push((
        <div key="login-form-result">Login Successful!</div>
      ));
    } else {
      if ((error || formError) && !loading) {
        form.push((
          <div key="login-form-error" className={"alert alert-danger"}>
            There was an error with your login: {error || formError}
          </div>
        ));
      }
      form.push((
        <div key="login-form">
          <Form>
          <FormGroup>
            <Label for="emailAddress">Email</Label>
            <Input type="email" name="email" id="emailAddress" placeholder="example@somewhere.com" value={email} onChange={this.handleChange} onKeyPress={this.handleKeypress.bind(this)} />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password" name="password" id="password" placeholder="Super secret password" value={password} onChange={this.handleChange} onKeyPress={this.handleKeypress.bind(this)} />
          </FormGroup>
          <FormGroup>
            <Button color="success" onClick={() => this.loginUserDispatch(email, password)}>Submit</Button>
            </FormGroup>
            <Button color="light btn-sm" onClick={() => this.resetPassword(email)}>I Lost My Password</Button>

          </Form>
        </div>
      ));
    }
    return form;
  }
}

const mapStateToProps = (state) => {
  const UserLogin = {
    result: state.UserLogin.result,
    loading: state.UserLogin.loading,
    error: state.UserLogin.error,
    token: (state.UserLogin.result ? state.UserLogin.result.token : null),
    role: (state.UserLogin.result ? state.UserLogin.result.role : null)
  }
  if (state.UserLogin.result && state.UserLogin.result.sponsor_info) {
      UserLogin.sponsor_info = state.UserLogin.result.sponsor_info;
  }
	return {
    ResetPasswordGet: {
      result: state.ResetPasswordGet.result,
      loading: state.ResetPasswordGet.loading,
      error: state.ResetPasswordGet.error
    },
    UserLogin
  }
}

export default connect(mapStateToProps)(LoginForm);