import React, { Component } from 'react';
import {connect} from "react-redux";
import {confirmUserRegistration} from "../../../actions/Users/confirmregistration";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class confirmRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    console.log({qry: this.props.query})
    if(this.props.query && this.props.query.confirmationCode) {
      this.setState({confirming: true});
      this.props.dispatch(confirmUserRegistration(this.props.query.confirmationCode));
    } else {
      this.setState({confirmationError: "No confirmation code present"})
    }
  }

  render() {
    const {result, loading, error} = this.props.ConfirmRegistration;
    const confirmationError = this.state.confirmationError;
    if (confirmationError) return (<div>{confirmationError}</div>);
    if (loading) return (<div>Confirming registration code...</div>);
    if (!loading && error) return (<div>{error}</div>);
    if (!loading && result) {
      if (result.confirmed) {
        return (
          <div>Your account has been confirmed! You may now log in.</div>
        )
      } else {
        return (
          <div>Your account could not be confirmed; please contact support.</div>
        )
      }

    }
  }
}

const mapStateToProps = (state) => {
	return {
	  ConfirmRegistration: {
      result: state.ConfirmRegistration.result,
      loading: state.ConfirmRegistration.loading,
      error: state.ConfirmRegistration.error
    }
	};
}

export default connect(mapStateToProps)(confirmRegistration);