import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getQuestionById} from "../../../../actions/Questions/getById";
import {updateAnswer} from "../../../../actions/Answers/update";
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionText: "",
      questionId: 0,
      answerText: "",
      answerBoolean: -1,
      answerMultiple: 0,
      answered: false,
      firstRequest: false,
      formError: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
    if (target.dataset.answerText) {
      this.setState({answerText: target.dataset.answerText});
    }
  }

  getQuestionDispatch() {
    const state = this.state;
    const {question, answer, qid, show} = this.props;
    if (isNaN(qid) || qid < 1) return;
    this.props.dispatch(getQuestionById(qid));
  }

  async saveAnswerDispatch() {
    const q = this.props.question;
    const loaded = q;
    if (loaded) {
      // After we verify the question loaded, we need to make sure that we have an answer,
      // and set an error if we don't
      if (q.questionType === "text" && this.state.answerText.length < 1) {
        this.setState({formError: "You did not submit an answer."})
        return;
      } else if (q.questionType === "boolean" && this.state.answerBoolean < 0) {
        this.setState({formError: "You did not submit an answer."})
        return;
      } else if (q.questionType === "multiple_choice" && this.state.answerMultiple.length < 1) {
        this.setState({formError: "You did not submit an answer."})
        return;
      } else if (!q.questionId) {
        this.setState({formError: "An invalid question was loaded. Please contact support."})
        return;
      }
      const answerObj = {
        questionId: q.questionId,
        type: q.questionType
      }
      // We should have answer text regardless of the type of
      // question, so we don't need to evaluate type here
      if (this.state.answerText.length > 0) {
        answerObj.answerText = this.state.answerText;
      }
      if (q.questionType === "boolean") {
        answerObj.answerBoolean = this.state.answerBoolean;
      } else if (q.questionType === "multiple_choice") {
        answerObj.answerMultiple = this.state.answerMultiple;
      }
      await this.props.dispatch(updateAnswer(answerObj));
    }
  }

  componentDidMount(){
    const {show} = this.props;
    if (show) this.getQuestionDispatch();
  }

  componentDidUpdate(prevProps){
    const {question, answer, show, currentAnswer, currentAnswerId} = this.props;
    if (prevProps.show !== show) {
      this.setState({show});
      this.getQuestionDispatch();
    }
    if (prevProps.answer.loading && !answer.loading) {
      this.clearQuestionAndAnswer();
      // Display a success alert if a user has successfully answered a question
      this.setState({answered: true});
      // Only show the answered bubble for 2.5 seconds
      setTimeout(() => {
        this.closeQuestion();
      }, 1500);
    }
    if (prevProps.question.loading && !question.loading) {
      if (question.questionType === "text") {
        this.setState({answerText: currentAnswer});
      }
      if (question.questionType === "boolean") {
        this.setState({answerBoolean: currentAnswer === "Yes" ? 1 : 0});
      }
      if (question.questionType === "multiple_choice") {
        this.setState({answerMultiple: parseInt(currentAnswerId)});
      }
    }
  }

  clearQuestionAndAnswer() {
    this.setState({question: {}, answer: {}, answerText: "", answerBoolean: -1, answerMultiple: 0, questionText: "", questionId: 0, answered: false});
  }

  openQuestion() {
    this.setState({show: true});
  }

  closeQuestion() {
    // Ensure that we don't have any values saved
    // in case a user comes back to this screen
    this.clearQuestionAndAnswer();
    // Function to pass back to talk to the parent of this component
    // to let it know that we've closed
    if (this.props.closeParent) this.props.closeParent();
    this.setState({show: false});
  }

  render() {
    const {answerText, answerBoolean, answerMultiple, answered, formError, show} = this.state;
    const {render, buttonText, question, answer, currentAnswer} = this.props;
    const {questionText, questionId, questionType, possibleAnswers, result, loading, error} = question;

    let renderObj = [];

    if (render === "button") {
      renderObj.push((<Button key="get-question" onClick={this.openQuestion.bind(this)}>{buttonText || "New Question"}</Button>));
    } else if (render === "link") {
      renderObj.push((<a key="get-question" href="#" onClick={this.openQuestion.bind(this)}>{buttonText || "New Question"}</a>));
    }

    let form = []

    // Set our form title up, but set our answer success div up as well; this allows us to set a class
    // on it to fade in and out
    if (answered) {
      form.push(
        (
          <div key="answer-success" className={"answer-success " + (!answered ? "hidden" : "alert alert-success")}>Answer successfully updated!</div>
        )
      );
    } else {
      form.push(
        (
          <div key="reg-form-title"><h3 className="form-title">A Question...</h3></div>
        )
      );
    };

    // Set up our button nav
    const nav = [(
      <div key="button-control-panel" className="button-control-panel">
        <Button className={"use-image submit"} onClick={this.saveAnswerDispatch.bind(this)}></Button>
        <Button className="use-image close-question" onClick={this.closeQuestion.bind(this)}></Button>
      </div>
    )];

    const formErrorContainer = [];
    if (formError) {
      formErrorContainer.push((
        <div key="form-error" className="alert alert-danger">{formError}</div>
      ));
    }

    const answerError = [];
    if (answer.error) {
      answerError.push((
        <div key="answer-error" className="alert alert-danger">{answer.error}</div>
      ));
    }
    if (!answered) {
      // If we haven't answered the question, display the form
      if (loading) {
        form.push((
          <div key="question-form-loading">Loading Question</div>
        ));
      } else if (!loading && !error && questionId && questionId > 0) {
        // Set up the type of input we expect to see based on the question
        const answerInput = [];
        if (questionType === "text") {
          answerInput.push((<textarea className="form-control" name="answerText" key="answerText" id="answerText" placeholder="My answer is..." value={answerText} onChange={this.handleChange} />));
        } else if (questionType === "boolean") {
          // Set up the two answers we need: true and false
          answerInput.push((<div key="answerBooleanTrue"><input type="radio" className="form-control" name="answerBoolean" id="answerBooleanTrue" value="1" onChange={this.handleChange} checked={answerBoolean === 1} data-answer-text="Yes" /> Yes </div>));
          answerInput.push((<div key="answerBooleanFalse"><input type="radio" className="form-control" name="answerBoolean" id="answerBooleanFalse" value="0" onChange={this.handleChange} checked={answerBoolean === 0} data-answer-text="No" /> No </div>));
        } else if (questionType === "multiple_choice") {
          // Loop through our possible answers and set up the radios for them
          possibleAnswers.forEach((answer, i) => {
            answerInput.push((<div key={"answerMultiple-" + i}><input type="radio" className="form-control" name="answerMultiple" id={"answerMultiple-" + i} value={answer.id} onChange={this.handleChange} checked={answerMultiple === answer.id} data-answer-text={answer.answer_text} /> <label htmlFor={"answerMultiple-" + i}>{answer.answer_text}</label> </div>));
          });
        }

        form.push((
          <div key="question-form">

            <div className="question-form-result" id="question-form-result"> {questionText}</div>
            <Form>

              {answerError}
              {formErrorContainer}
              {answerInput}

            {nav}
            </Form>
          </div>
        ));
      } else {
        if ((error || formError) && !loading) {
          form.push((
            <div key="question-form-error" className={"alert alert-danger"}>
              There was an error getting a question: {error || formError}
            </div>
          ));
        }
      }
    }
    const formHolder = [
      (
        <div key="question-form-holder" id="question-form-holder" className="container qaform">{form}</div>
      )
    ];
    return [
      renderObj,
      (
        <div key="question-form-container" id="question-form-container" className={!show ? "hidden" : ""}>{formHolder}</div>
      )
    ];
  }
}

const mapStateToProps = (state) => {
  let questionId = 0,
      questionText = "",
      questionType = "",
      possibleAnswers = [],
      getQ = state.GetQuestionById,
      updateA = state.UpdateAnswer;
  if (getQ.result) {
    const q = getQ.result;
    questionId = q.id;
    questionText = q.text;
    questionType = q.type;
    possibleAnswers = q.possible_answers;
  }
	return {
    question: {
      result: getQ.result,
      questionId,
      questionText,
      questionType,
      possibleAnswers,
      loading: getQ.loading,
      error: getQ.error
    },
    answer: {
      result: updateA.result,
      loading: updateA.loading,
      error: updateA.error
    }
	};
}

export default connect(mapStateToProps)(Editor);