import React, { Component } from 'react';
import {connect} from "react-redux";
import Router from 'next/router';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getUserProfile, getUserProfileClearState} from "../../../../actions/Users/getProfile";
import {updateUserProfile, updateUserProfileClearState} from "../../../../actions/Users/updateProfile";
import config from '../../../../env';
import './style.scss';

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userFirstName: "",
      userLastName: ""
    }
  }

  getProfileImage(userInfo) {
    if (!userInfo.profile_image || !userInfo.profile_image.length) return config.default_profile_image;
    return userInfo.profile_image;
  }

  async handleUpload(e) {
    const file = Array.from(e.target.files).shift();
    this.setState({ uploading: true });

    const getBase64Img = new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      if (file) {
        reader.readAsDataURL(file);
      }
    });
    const img = await getBase64Img;
    const disp = await this.props.dispatch(updateUserProfile({profile_image: img}));
    await this.props.dispatch(getUserProfile());
    this.setState({ uploading: false, editPic: false });
    return disp;
  }

  async handleChange(event) {
    // Update our formError to clear out when we change
    // an input value, so we aren't taking up a buttload
    // of form space with an error if somebody is trying
    // to fix it
    this.setState({formError: ""});
    // Process our change as normal
    const target = event.target;
    let name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
  }

  async saveInfo() {
    this.setState({saving: true})
    const {userFirstName, userLastName} = this.state;
    const saveable = {};
    if (userFirstName && userFirstName.length) {
      saveable.first_name = userFirstName;
    }
    if (userLastName && userLastName.length) {
      saveable.last_name = userLastName;
    }
    if (!Object.keys(saveable).length) {
      this.setState({saving: false})
      return false;
    }
    await this.props.dispatch(updateUserProfile(saveable));
    await this.props.dispatch(getUserProfile());
    this.setState({saving: false, editUser: false});
  }

  editPicInterface() {
    return (
      <div className='edit-pic'>
        <h4>Upload Image</h4>
        <div>
          <input type='file' onChange={this.handleUpload.bind(this)} disabled={this.state.uploading}/>
        </div>
        <button  type="button" className={"right profile-edit open use-image close-question btn btn-secondary"} onClick={this.toggleEditPic.bind(this)}></button>
      </div>
    );
  }

  editUserInterface() {
    const {result} = this.props.userProfile;
    const user = result.profile.user;
    return (
      <div className='edit-user'>
        <h3>Edit User Info</h3>
        <div>
          <input className='form-control' type='text' value={this.state.userFirstName || user.first_name} name='userFirstName' onChange={this.handleChange.bind(this)} disabled={this.state.saving}/>
          <input className='form-control' type='text' value={this.state.userLastName || user.last_name} name='userLastName' onChange={this.handleChange.bind(this)} disabled={this.state.saving}/>
          <button className='right use-image submit btn btn-secondary' type='button' onClick={this.saveInfo.bind(this)} disabled={this.state.saving}></button>
          <button  type="button" className={"right profile-edit open use-image close-question btn btn-secondary"} onClick={this.toggleEditUserData.bind(this)}></button>
        </div>
      </div>
    );
  }

  toggleEditPic() {
    this.setState({editPic: !this.state.editPic, editUser: false});
  }

  toggleEditUserData() {
    this.setState({editUser: !this.state.editUser, editPic: false});
  }

  componentDidMount() {
    this.props.dispatch(getUserProfile());
  }

  render() {
    const {result, error, loading} = this.props.userProfile;
    const {editable} = this.props;
    const {editPic, editUser} = this.state;
    // If we don't have user data yet, there is nothing to show
    if (!result || !result.profile || !result.profile.user || loading) return (<div></div>);
    if (error) return(<div>There was an error loading the answered questions: {error}</div>);
    const profile = result.profile;
    const userInfo = profile.user;
    const editableHeader = editable ? (<h3>Your Profile</h3>) : "";
    const profilePicEdit = editable ? (<button  type="button" className={editPic ? "right profile-edit open use-image close-question btn btn-secondary" : "topright profile-edit editIcon btn btn-secondary"} onClick={this.toggleEditPic.bind(this)}></button>) : "";
    const userInfoEdit = editable ? (<button  type="button" className={editUser ? "right profile-edit open use-image close-question btn btn-secondary" : "topright profile-edit editIcon btn btn-secondary"} onClick={this.toggleEditUserData.bind(this)}></button>) : "";
    // If we *do* have user info, we need to display it
    return (
      <div className="profileWrapper popbox">
      <div className="user-data">
          <h3 className="user-full-name"><span className="user-info first-name">{userInfo.first_name}</span> <span className="user-info last-name">{userInfo.last_name}</span></h3>
          <div className="user-info email">{userInfo.email}</div>
          {userInfoEdit}
        </div>
        
        <div className="profilePic"><img src={this.getProfileImage(userInfo)} />{profilePicEdit}</div>
        {editPic && !editUser ? this.editPicInterface() : ""}
        {editUser && !editPic ? this.editUserInterface() : ""}
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      userProfile: {
        result: state.UserProfile.result,
        loading: state.UserProfile.loading,
        error: state.UserProfile.error
      },
      userProfileUpdate: {
        result: state.UpdateUserProfile.result,
        loading: state.UpdateUserProfile.loading,
        error: state.UpdateUserProfile.error
      }
    };
}

export default connect(mapStateToProps)(UserInfo);