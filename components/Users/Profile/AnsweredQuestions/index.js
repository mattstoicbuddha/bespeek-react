import React, { Component } from 'react';
import {connect} from "react-redux";
import Router from 'next/router';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getAnswers, getAnswersClearState} from "../../../../actions/Answers/get";
import Editor from '../QuestionEditor';
import './style.scss';

class AnsweredQuestions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditor: false,
      qid: 0,
      answer: "",
      answerId: 0
    }
  }

  componentDidMount() {
    this.props.dispatch(getAnswers());
  }

  componentDidUpdate(prevProps) {
    if (this.props.saveA) {
      const {result, loading, error} = this.props.saveA;
      if (prevProps.saveA.loading && !loading) {
        // We were saving an answer, now we aren't
        if (result) {
          // Since we were saving an answer and now aren't,
          // we know we have new answer data and need to reload it
          this.props.dispatch(getAnswers());
        }
      }
    }
  }

  reloadQuestions(qid, answer, answerId) {
    this.toggleEditor(qid, answer, answerId);
    this.props.dispatch(getAnswers());
  }

  toggleEditor(qid, answer, answerId) {
    this.setState({showEditor: !this.state.showEditor, qid, answer, answerId});
  }

  render() {
    const {result, error, loading} = this.props;
    const {showEditor, qid, answer, answerId} = this.state;
    // If we don't have answer data yet, there is nothing to show
    if (!result || !result.answers || loading) return (<div></div>);
    if (error) return(<div>There was an error loading the answered questions: {error}</div>);
    // If we *do* have answer data, we need to display it
    const answers = [];
    // Get all of our answers together
    result.answers.forEach((info, i) => {
      answers.push((
        <div className="qaGroup popbox" key={"answer-" + i}>
        <div className="row">
          <div className="col-md-10">
            <div className="question">{info.question}</div>
            <div className="answer">{info.answer}</div>
          </div>
          <div className="col-md-2">
            <div className="change"><Button className="editIcon" onClick={this.toggleEditor.bind(this, info.qid, info.answer, info.possible_answer_id)}></Button></div>
          </div>
          </div>
        </div>
      ));
    });
    return (
      <div>
        <h3>Questions Answered</h3>
        {answers}
        <Editor show={showEditor} qid={qid} currentAnswer={answer} currentAnswerId={answerId} closeParent={this.reloadQuestions.bind(this, 0, "", 0)} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    const saveA = state.SaveAnswer;
    return {
      result: state.GetAnswers.result,
      loading: state.GetAnswers.loading,
      error: state.GetAnswers.error,
      saveA
    };
}

export default connect(mapStateToProps)(AnsweredQuestions);