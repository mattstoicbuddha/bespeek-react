import React, { Component } from 'react';
import {connect} from "react-redux";
import Router from 'next/router';
import { Button, Form, FormGroup, Label, Input, FormText, UncontrolledCollapse, CardBody, Card } from 'reactstrap';
import BespeekTooltip from '../../../Layout/tooltip';
import {addConnection, getConnections, deleteConnection} from "../../../../actions/Users/Connections";
import config from '../../../../env';
import './style.scss';

class UserConnections extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAddField: false,
      connectionEmail: "",
      addedConnection: null
    }
  }

  componentDidUpdate(prevProps) {
    const {AddConnections} = this.props;
    const prevConnections = prevProps.AddConnections;
    if (prevConnections.loading && !AddConnections.loading) {
      if (AddConnections.error) {
        console.log("BOOO", AddConnections.error);
      } else if (AddConnections.result) {
         this.setState({showAddField: false})
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(getConnections());
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
  }

  addConnectionModal() {
    this.setState({showAddField: true, addedConnection: null, connectionEmail: ""})
  }

  hideConnectionModal() {
    this.setState({showAddField: false})
  }

  async deleteConnectionDispatch(id, email) {
    if(!window.confirm("Are you sure you would like to delete " + email + " from your connections?")) return;
    await this.props.dispatch(deleteConnection(id));
    await this.props.dispatch(getConnections());
  }

  async addConnectionDispatch() {
    const connObj = {
      email: this.state.connectionEmail
    }
    await this.props.dispatch(addConnection(connObj));
    if (!this.props.AddConnections.error) {
      this.hideConnectionModal();
    }
    this.setState({addedConnection: true});
    await this.props.dispatch(getConnections());
  }

  render() {
    const { showAddField, addedConnection } = this.state;
    const {UserProfile, AddConnections, GetConnections} = this.props;
    const profileResults = UserProfile.result;
    const connections = GetConnections.result && GetConnections.result.connections || null;
    let addConnectionResult = (<div></div>),
        personalConnectionDisplay = (<div></div>),
        remoteConnectionDisplay = (<div></div>);
    if (addedConnection && !AddConnections.loading && (AddConnections.result || AddConnections.error)) {
      const addConnResult = AddConnections.error || AddConnections.result;
      const connResultClass = !isNaN(addConnectionResult) ? "conn-success" : "conn-failed";
      addConnectionResult = (<div className={'add-connection-result ' + connResultClass}>{addConnResult.success ? "Successfully Added Connection" : "Failed to add connection: " + addConnResult.error}</div>);
    }
    let addConnectionField = (<div></div>);
    if (showAddField) {
      addConnectionField = (<div className='add-connection-field'>
        {addConnectionResult}
        <h4>Add your key holder's email address</h4>
        <button className='topright use-image close-question btn btn-secondary close-connections-button' onClick={this.hideConnectionModal.bind(this)}></button>
        <div className="input-group mb-3">
        <input className="form-control" type='text' name='connectionEmail' value={this.state.connectionEmail} onChange={this.handleChange.bind(this)}/>
        <div className="input-group-append">
        <button className='right btn btn-secondary submit-connection-button' onClick={this.addConnectionDispatch.bind(this)}>Submit</button>
        
        </div>
        </div>
        
      </div>);
    }
    if (connections && connections.length) {
      personalConnectionDisplay = connections.filter(c => c.added_by_user || c.users_connected).map((conn, i) => {
        return (
          <div key={'connection-' + i} className='connection-wrapper'>
            <div className='email'>{conn.email}</div>
            <div className='delete'>
              <button type='button' className="use-image close-question btn btn-secondary" onClick={this.deleteConnectionDispatch.bind(this, conn.user_id, conn.email)}> </button>
            </div>
          </div>
        )
      });

      remoteConnectionDisplay = connections.filter(c => !c.added_by_user || c.users_connected).map((conn, i) => {
        return (
          <div key={'connection-' + i} className='connection-wrapper'>
            <div className='email'><a href={'/connections/profile/' + conn.user_id}>{conn.email}</a></div>
          </div>
        )
      });
    }
    // If we *do* have user info, we need to display it
    return (

      <div className={"popbox connections " + (showAddField ? " show-add-connection" : "")}>
      <div className="space-between">
        <div>
          <h3>My Key Holders<span href="#" id="keyholders" className="bespeek-tooltip"> ? </span></h3>
          <BespeekTooltip target="keyholders" placement="right">
            Key Holders are important because they are the people who will be able to view your profile after your passing.
          </BespeekTooltip>
        </div>
        <div>
          <button className=' btn btn-outline-secondary btn-sm add-connection' onClick={this.addConnectionModal.bind(this)}>+ Key Holders</button>
        </div>
      </div>
        {addConnectionField}

        {personalConnectionDisplay}

  <div className="keys-section">
  <h3 class="tog" id="keystoggler">My Keys <span class="material-icons">expand_more</span><span href="#" id="keys" className="bespeek-tooltip"> ? </span></h3>
   
    <UncontrolledCollapse toggler="#keystoggler">
      <Card>
        <CardBody>
          <BespeekTooltip target="keys" placement="right">
            Keys are important because they are the people whos profiles you will be able to access after thier passing.
          </BespeekTooltip>
          {remoteConnectionDisplay}
        </CardBody>
      </Card>
    </UncontrolledCollapse>
  </div>







      </div>
    );
    return (<div></div>);
  }
}

const mapStateToProps = (state) => {
    return {
        AddConnections: {
          result: state.AddConnections.result,
          loading: state.AddConnections.loading,
          error: state.AddConnections.error
        },
        GetConnections: {
          result: state.GetConnections.result,
          loading: state.GetConnections.loading,
          error: state.GetConnections.error
        },
        UserProfile: {
          result: state.UserProfile.result,
          loading: state.UserProfile.loading,
          error: state.UserProfile.error
        }
    };
}

export default connect(mapStateToProps)(UserConnections);