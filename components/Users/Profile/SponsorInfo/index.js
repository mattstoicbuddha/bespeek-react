import React, { Component } from 'react';
import {connect} from "react-redux";
import Router from 'next/router';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import config from '../../../../env';
import './style.scss';

class UserInfo extends Component {
  constructor(props) {
    super(props);
  }

  getLogoImage(sponsorInfo) {
    if (!sponsorInfo.logo || !sponsorInfo.logo.length) return config.default_profile_image;
    return sponsorInfo.logo;
  }

  render() {
    const {result} = this.props;
    // Nothing to show
    if (!result || !result.profile || !result.profile.sponsor) return (<div></div>);
    const sponsorInfo = result.profile.sponsor;
    

    // If we *do* have user info, we need to display it
    return (
      <div className="sponsorInfoWrapper popbox">
        <div className="logo"><img src={this.getLogoImage(sponsorInfo)} /></div>
        <div>
          <h3>This Member is Proudly Sponsored By:</h3>
          <div className="sponsor_name">
            <span className="sponsor-info name">{sponsorInfo.company_name}</span>
            <div className="company-number"><span>Phone:</span> {sponsorInfo.contact_number}</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      result: state.UserProfile.result,
      loading: state.UserProfile.loading,
      error: state.UserProfile.error
    };
}

export default connect(mapStateToProps)(UserInfo);