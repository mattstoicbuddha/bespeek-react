import React, { Component } from 'react';
import {useRouter} from 'next/router';
import Link from 'next/link';
import {connect} from "react-redux";
import {getPasswordResetCheck} from "../../../actions/Users/resetpassword/check";
import {savePasswordReset} from "../../../actions/Users/resetpassword/save";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './style.scss';

class ResetPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password_one: "",
      password_two: "",
      updatingPass: false,
      passwordUpdated: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if(this.props.query && this.props.query.code) {
      this.setState({confirming: true});
      this.props.dispatch(getPasswordResetCheck(this.props.query.code));
    } else {
      this.setState({confirmationError: "No password reset code present"})
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.ResetPasswordSave.loading) {
      const {ResetPasswordSave} = this.props;
      if (!ResetPasswordSave.loading) {
        this.setState({updatingPass: false});
        if (ResetPasswordSave.error) {
          alert("There was an error resetting your password: " + ResetPasswordSave.error);
          return;
        }
        this.setState({passwordUpdated: true});
      }
    }
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({[name]: value});
  }

  updatePassword(password_one, password_two) {
    this.setState({updatingPass: true});
    this.props.dispatch(savePasswordReset({password_first: password_one, password_second: password_two, reset_code: this.props.query.code}));
  }

  render() {
    const {password_one, password_two, updatingPass, passwordUpdated} = this.state;
    const {ResetPasswordSave, ResetPasswordCheck} = this.props;
    if (ResetPasswordCheck.loading) {
      return (<div>Checking password reset code...</div>);
    }
    if (!ResetPasswordCheck.loading && ResetPasswordCheck.error) {
      return (<div>There was an error verifying your password reset code: {ResetPasswordCheck.error}</div>);
    }
    if (passwordUpdated) {
      return (<div><div>Your password has been updated!</div> <Link route='/users/login/' prefetch><a>Log In Here</a></Link></div>);
    }
    let form = [(
      <div key="reg-form-title"><h3 className="form-title">Reset Password</h3></div>
    )];
    form.push((
      <div key="reg-form">
        <Form>
          <FormGroup>
            <Label for="password_one">New Password</Label>
            <Input type="password" name="password_one" id="password_one" placeholder="New Password" value={password_one} disabled={updatingPass} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup>
            <Label for="password_two">Confirm Password</Label>
            <Input type="password" name="password_two" id="password_two" placeholder="Confirm Password" value={password_two} disabled={updatingPass} onChange={this.handleChange} />
          </FormGroup>
          <Button color="success" disabled={updatingPass} onClick={() => this.updatePassword(password_one, password_two)}>Submit</Button>
        </Form>
      </div>
    ));
    return form;
  }
}

const mapStateToProps = (state) => {
	return {
	  ResetPasswordSave: {
      result: state.ResetPasswordSave.result,
      loading: state.ResetPasswordSave.loading,
      error: state.ResetPasswordSave.error
    },
    ResetPasswordCheck: {
      result: state.ResetPasswordCheck.result,
      loading: state.ResetPasswordCheck.loading,
      error: state.ResetPasswordCheck.error
    }
	};
}

export default connect(mapStateToProps)(ResetPasswordForm);