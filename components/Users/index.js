import Story from "./Story"
import ResetPasswordForm from "./ResetPasswordForm"
import RegistrationForm from "./RegistrationForm"
import Question from "./Question"
import Profile from "./Profile"
import LoginForm from "./LoginForm"
import CreatePassword from "./CreatePassword"
import ConfirmRegistrationCheck from "./ConfirmRegistrationCheck"

export {
	Story,
	ResetPasswordForm,
	RegistrationForm,
	Question,
	Profile,
	LoginForm,
	CreatePassword,
	ConfirmRegistrationCheck
}