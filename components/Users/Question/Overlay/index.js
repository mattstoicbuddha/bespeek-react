import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getQuestion} from "../../../../actions/Questions/get";
import {saveAnswer} from "../../../../actions/Answers/save";
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class Overlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionText: "",
      questionId: 0,
      answerText: "",
      answerBoolean: -1,
      answerMultiple: 0,
      answered: false,
      firstRequest: false,
      formError: "",
      showCatUnlocked: false,
      answerCategoryInfo: {},
      getNextQuestion: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
    if (target.dataset.answerText) {
      this.setState({answerText: target.dataset.answerText});
    }
  }

  getQuestionDispatch() {
    const state = this.state;
    const {question, answer} = this.props;
    if (state.firstRequest && !question.questionLoaded) {
      // If we have already done a request and our questionLoaded prop
      // isn't true, then one of two things has happened:
      // 1. The initial request hasn't finished yet.
      // 2. The initial request finished and we have no questions.
      // Since questions are not currently added with the kind of frequency
      // that demands we check for new questions regularly, we can safely
      // ignore the request to get a new question
      return;
    }
    if (!state.firstRequest) {
      // Ensure that we know we have already done a request
      // to get a question; if we have no questions after
      // that, such is life
      this.setState({firstRequest: true})
    }

    this.props.dispatch(getQuestion());
  }

  saveAnswerDispatch() {
    const q = this.props.question;
    const loaded = q && q.questionLoaded;
    if (loaded) {
      // After we verify the question loaded, we need to make sure that we have an answer,
      // and set an error if we don't
      if (q.questionType === "text" && this.state.answerText.length < 1) {
        this.setState({formError: "You did not submit an answer."})
        return;
      } else if (q.questionType === "boolean" && this.state.answerBoolean < 0) {
        this.setState({formError: "You did not submit an answer."})
        return;
      } else if (q.questionType === "multiple_choice" && this.state.answerMultiple.length < 1) {
        this.setState({formError: "You did not submit an answer."})
        return;
      } else if (!q.questionId) {
        this.setState({formError: "An invalid question was loaded. Please contact support."})
        return;
      }
      const answerObj = {
        questionId: q.questionId,
        type: q.questionType
      }
      // We should have answer text regardless of the type of
      // question, so we don't need to evaluate type here
      if (this.state.answerText.length > 0) {
        answerObj.answerText = this.state.answerText;
      }
      if (q.questionType === "boolean") {
        answerObj.answerBoolean = this.state.answerBoolean;
      } else if (q.questionType === "multiple_choice") {
        answerObj.answerMultiple = this.state.answerMultiple;
      }
      this.props.dispatch(saveAnswer(answerObj));
    }
  }

  componentDidMount(){
    if (this.state.show) this.getQuestionDispatch();
  }

  componentDidUpdate(prevProps){
    const {question, answer, show} = this.props;
    if (prevProps.show !== show) {
      this.setState({show});
    }
    if (this.props.show && !this.props.question.questionLoaded) {
      this.getQuestionDispatch();
    }
    if (prevProps.answer.loading && !answer.loading) {
      this.clearQuestionAndAnswer();
      if (answer.result && answer.result.category_unlocked) {
        this.setState({showCatUnlocked: true, answerCategoryInfo: answer.result.category});
        return;
      }
      // Display a success alert if a user has successfully answered a question
      this.setState({answered: true});
      this.getQuestionDispatch();
      // Only show the answered bubble for 2.5 seconds
      setTimeout(() => {
        this.setState({answered: false})
      }, 2500);
    }
    if (this.state.getNextQuestion) {
      this.setState({getNextQuestion: false});
      this.getQuestionDispatch();
    }
  }

  clearQuestionAndAnswer() {
    this.setState({question: {}, answer: {}, answerText: "", answerBoolean: -1, answerMultiple: 0, questionText: "", questionId: 0, answered: false});
  }

  openQuestion() {
    this.setState({show: true});
  }

  closeQuestion() {
    this.setState({show: false});
    // Function to pass back to talk to the parent of this component
    // to let it know that we've closed
    if (this.props.closeParent) this.props.closeParent();
    // Ensure that we don't have any values saved
    // in case a user comes back to this screen
    this.clearQuestionAndAnswer();
  }

  render() {
    const {answerText, answerBoolean, answerMultiple, answered, formError, show, showCatUnlocked, answerCategoryInfo} = this.state;
    const {render, buttonText, question, answer} = this.props;
    const {questionText, questionId, questionType, possibleAnswers, questionLoaded, result, loading, error} = question;

    let renderObj = [];

    if (render === "button") {
      renderObj.push((<Button key="get-question" onClick={this.openQuestion.bind(this)}>{buttonText || "New Question"}</Button>));
    } else if (render === "link") {
      renderObj.push((<a key="get-question" href="#" onClick={this.openQuestion.bind(this)}>{buttonText || "New Question"}</a>));
    }

    let form = []

    // Set our form title up, but set our answer success div up as well; this allows us to set a class
    // on it to fade in and out
    form.push(
      (
        <div key="answer-success" className={"answer-success " + (!answered ? "hidden" : "alert alert-success")}>Question successfully answered!</div>
      ),
      (
        <div key="reg-form-title"><h3 className="form-title">A Question...</h3></div>
      )
    );

    // Set up our button nav
    const nav = [(
      <div key="button-control-panel" className="button-control-panel">
        <Button className={"use-image submit" + (!questionLoaded ? " hidden" : "")} onClick={this.saveAnswerDispatch.bind(this)}></Button>
        <Button className={"use-image new-question" + (!questionLoaded ? " hidden" : "")}  onClick={this.getQuestionDispatch.bind(this)}></Button>
        <Button className="use-image close-question" onClick={this.closeQuestion.bind(this)}></Button>
      </div>
    )];

    const formErrorContainer = [];
    if (formError) {
      formErrorContainer.push((
        <div key="form-error" className="alert alert-danger">{formError}</div>
      ));
    }

    const answerError = [];
    if (answer.error) {
      answerError.push((
        <div key="answer-error" className="alert alert-danger">{answer.error}</div>
      ));
    }

    const questionCategoryUnlock = [];

    if (showCatUnlocked) {
     
      console.log({answerCategoryInfo})
      const catDiv = (
       
        <div className='unlocked-category' key='unlocked-category'>
          <div className='category-unlock-message'>
            <h4>Congratulations!</h4> You've unlocked:
         
          <div className='category-name'><h3>{answerCategoryInfo.category_name}</h3></div>
          <div className='category-description'>{answerCategoryInfo.category_description}</div>
          <div className='unlocked-close' onClick={() => this.setState({showCatUnlocked: false, getNextQuestion: true})}>Awesome!</div>
           </div>
        </div>
        
      );
      questionCategoryUnlock.push(catDiv);
    }

    if (loading) {
      form.push((
        <div key="question-form-loading">Loading Question</div>
      ));
    } else if (!loading && !error && questionId && questionId > 0) {
      // Set up the type of input we expect to see based on the question
      const answerInput = [];
      if (questionType === "text") {
        answerInput.push((<textarea className="form-control" name="answerText" key="answerText" id="answerText" placeholder="My answer is..." value={answerText} onChange={this.handleChange} />));
      } else if (questionType === "boolean") {
        // Set up the two answers we need: true and false
        answerInput.push((<div key="answerBooleanTrue"><input type="radio" className="form-control" name="answerBoolean" id="answerBooleanTrue" value="1" onChange={this.handleChange} checked={answerBoolean === 1} data-answer-text="Yes" /> Yes </div>));
        answerInput.push((<div key="answerBooleanFalse"><input type="radio" className="form-control" name="answerBoolean" id="answerBooleanFalse" value="0" onChange={this.handleChange} checked={answerBoolean === 0} data-answer-text="No" /> No </div>));
      } else if (questionType === "multiple_choice") {
        // Loop through our possible answers and set up the radios for them
        possibleAnswers.forEach((answer, i) => {
          answerInput.push((<div key={"answerMultiple-" + i}><input type="radio" className="form-control" name="answerMultiple" id={"answerMultiple-" + i} value={answer.id} onChange={this.handleChange} checked={answerMultiple === answer.id} data-answer-text={answer.answer_text} /> <label htmlFor={"answerMultiple-" + i}>{answer.answer_text}</label> </div>));
        });
      }

      form.push((
        <div key="question-form">

          <div className="question-form-result" id="question-form-result"> {questionText}</div>
          <Form>

            {answerError}
            {formErrorContainer}
            {answerInput}

          {nav}
          </Form>
        </div>
      ));
    } else if(!questionLoaded) {
      form.push((
        <div key="question-form-no-more-questions" className={"alert alert-info"}>
          There are no new questions available.
          {nav}
        </div>
      ));
    } else {
      if ((error || formError) && !loading) {
        form.push((
          <div key="question-form-error" className={"alert alert-danger"}>
            There was an error getting a question: {error || formError}
          </div>
        ));
      }
    }
    const formHolder = [
      (
        <div key="question-form-holder" id="question-form-holder" className="container qaform">{form}</div>
      )
    ];
    return [
      renderObj,
      (
        <div key="question-form-container" id="question-form-container" className={!show ? "hidden" : ""}>{questionCategoryUnlock}{formHolder}</div>
      )
    ];
  }
}

const mapStateToProps = (state) => {
  let questionId = 0,
      questionText = "",
      questionType = "",
      possibleAnswers = [],
      getQ = state.GetQuestion,
      saveA = state.SaveAnswer;
  if (getQ.result && getQ.result.question) {
    const q = getQ.result.question;
    questionId = q.id;
    questionText = q.text;
    questionType = q.type;
    possibleAnswers = q.possible_answers;
  }
	return {
    question: {
      result: getQ.result,
      questionId,
      questionText,
      questionType,
      possibleAnswers,
      questionLoaded: getQ.result.question_loaded,
      loading: getQ.loading,
      error: getQ.error
    },
    answer: {
      result: saveA.result,
      loading: saveA.loading,
      error: saveA.error
    }
	};
}

export default connect(mapStateToProps)(Overlay);