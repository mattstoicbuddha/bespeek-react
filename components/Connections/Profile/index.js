import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getConnectionProfile} from "../../../actions/Users/Connections/getProfile";
import cookieHelper from '../../../helpers/cookies';
import './style.scss';

class ConnectionProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  componentDidMount() {
    if(this.props.query && this.props.query.profileId) {
      this.props.dispatch(getConnectionProfile(this.props.query.profileId));
    } else {
      // this.setState({confirmationError: "No confirmation code present"})
    }
  }

  render(){

    const {result, error, loading} = this.props;
    if (loading || (!result || !result.user)) return (<div></div>);
    if (!loading && error) return(<div>There was an error loading this profile: {error}</div>);

    const {user, answers, sponsor} = result;
    console.log(sponsor)


     const answersHolder = [];
    // Get all of our answers together
    answers.forEach((info, i) => {
      answersHolder.push((
        <div key={"answer-" + i}>
          <div className="row">
            <div className="col-md-12 eachquestion">
              <div className="question">{info.question}</div>
              <div className="answer">{info.answer}</div>
            </div>
          </div>
        </div>
      ));
    });

    return (
      <div className="profileWrapper popbox">
        <div className="flexrow space-between">
          <div className="profilePic">
            <img className='img img-fluid' src={user.profile_image} />
          </div>
          <div className="sponsorwrap">
            <h3>This Member is Proudly Sponsored By:</h3>
            <div className="sponsor-info name">{sponsor.company_name}</div>
            <div className="sponsorlogo">
              <img className='img img-fluid' src={sponsor.logo} />
            </div>
            
          </div>
        </div>
        <div className="user-data">
          <h3 className="user-full-name"><span className="user-info first-name">{user.first_name}</span> <span className="user-info last-name">{user.last_name}</span></h3>
          <div className="user-info email">{user.email}</div>
          <div className="answersholder">{answersHolder}</div>
        </div>
      
      </div>
      )
  }

}

const mapStateToProps = (state) => {
    return {
      result: state.GetConnectionProfile.result,
      loading: state.GetConnectionProfile.loading,
      error: state.GetConnectionProfile.error
    };
}

export default connect(mapStateToProps)(ConnectionProfile);