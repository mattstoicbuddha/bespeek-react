import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './style.scss';

class OffCanvasNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: props.isOpen
    };
  }

  componentDidMount() {
    if (!this.state.mounted) this.setState({mounted: true});
  }

  toggle(e) {
    console.log({target: e.target})
  	const overlayClicked = e.target.classList.contains('bespeek-off-canvas-overlay');
    console.log({overlayClicked})
  	if (overlayClicked && this.props.onClose) {
  		this.props.onClose();
  	}
  }

  render() {
  	const {isOpen} = this.props;
  	const {mounted} = this.state; 
  	return (
  		<div className={'bespeek-off-canvas-overlay' + (isOpen ? ' open' : '')} onClick={this.toggle.bind(this)}>
			<div className={'bespeek-off-canvas' + (isOpen ? ' open' : '')}>
				{this.props.children}
			</div>
		</div>
  	);
  }
}

export default OffCanvasNavigation;