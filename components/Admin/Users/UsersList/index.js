import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, ButtonGroup} from 'reactstrap';
import {getUsersList} from "../../../../actions/Admin/Users/get";
import {reactivateUser} from "../../../../actions/Admin/Users/reactivate";
import {deactivateUser} from "../../../../actions/Admin/Users/deactivate";
import {resendConfirmation} from "../../../../actions/Users/resendconfirmation";
import {getPasswordReset} from "../../../../actions/Users/resetpassword/get";
import helpers from '../../../../helpers';
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class UsersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: {},
      error: null,
      loading: false,
      resettingPassword: false,
      reloading: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    // Update our formError to clear out when we change
    // an input value, so we aren't taking up a buttload
    // of form space with an error if somebody is trying
    // to fix it
    this.setState({formError: ""});
    // Process our change as normal
    const target = event.target;
    let name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
  }

  componentDidMount() {
    this.props.dispatch(getUsersList());
  }

  componentDidUpdate(prevProps) {
    if (prevProps.UsersList.loading) {
      if (!this.props.UsersList.loading) {
        this.setState({reloading: false});
      }
    }
    if (prevProps.PasswordReset.loading) {
      if (!this.props.PasswordReset.loading) {
        this.setState({resettingPassword: false});
        if (this.props.PasswordReset.error) {
          alert("Error sending password reset email: " + this.props.PasswordReset.error);
          return;
        }
        alert("Password reset sent!");
      }
    }
    if (prevProps.ReactivateUser.loading) {
      if (!this.props.ReactivateUser.loading) {
        this.setState({reactivatingUser: false});
        this.setState({reloading: true});
        this.props.dispatch(getUsersList());
      }
    }
    if (prevProps.DeactivateUser.loading) {
      if (!this.props.DeactivateUser.loading) {
        this.setState({deactivatingUser: false});
        this.setState({reloading: true});
        this.props.dispatch(getUsersList());
      }
    }
    if (prevProps.ResendConfirmation.loading) {
      if (!this.props.ResendConfirmation.loading) {
        this.setState({resendingConfirmation: false});
        if (this.props.ResendConfirmation.error) {
          alert("Error sending confirmation email: " + this.props.ResendConfirmation.error);
          return;
        }
        alert("Confirmation email resent!");
      }
    }
  }

  updateUserActivation(userId, deactivate) {
    console.log("Deactivating?", deactivate);
    if (deactivate) {
      if (this.state.deactivatingUser) {
        alert("Already deactivating a different account.");
        return;
      }
      this.setState({deactivatingUser: true});
      this.props.dispatch(deactivateUser(userId));
    } else {
      if (this.state.reactivatingUser) {
        alert("Already reactivating a different account.");
        return;
      }
      this.setState({reactivatingUser: true});
      this.props.dispatch(reactivateUser(userId));
    }
  }

  resendConfirmationEmail(email) {
    if (this.state.resendingConfirmation) {
      alert("Already resending confirmation for a different account.");
      return;
    }
    this.setState({resendingConfirmation: true});
    this.props.dispatch(resendConfirmation(email));
  }

  resetPassword(email) {
    if (this.state.resettingPassword) {
      alert("Already resetting password for a different account.");
      return;
    }
    this.setState({resettingPassword: true});
    this.props.dispatch(getPasswordReset(email));
  }

  render() {
    const {reloading} = this.state;
    const {error, loading, result} = this.props.UsersList;
    if (!reloading && (loading || !result || !result.users)) return (<div></div>)
    if (error) {
      return (<div>Error loading the user list: {error}</div>)
    }
    const resettingPassword = this.props.PasswordReset.loading;
    const resendingConfirmation = this.props.ResendConfirmation.loading;
    const usersList = result.users.map((user, i) => {
      const lastLoginIndex = Array.isArray(user.meta_data) ? user.meta_data.findIndex((meta) => meta.meta_key === "last_login_timestamp") : -1;
      let lastLogin = null;
      if (lastLoginIndex > -1) {
        const loginTime = new Date(parseInt(user.meta_data[lastLoginIndex].meta_value));
        lastLogin = (loginTime.getMonth() + 1) + "/" + loginTime.getDate() + "/" + loginTime.getFullYear() + " " + loginTime.getHours() + ":" + loginTime.getMinutes();
      }

      let answerCount=(<div></div>);
      if (user.type==="member"){
        answerCount=(<div className="user-answer-count"><span>Answers </span>{user.answer_count}  </div>)
      }

      return (
        <div key={"userlist-" + i} className={'user-list user ' + (user.deactivated ? 'deactivated' : '')}>
          <div className="user-list-row">
            <div className="user-meta-wrapper">
              <div className="user-name"><span>User Name </span>{user.first_name} {user.last_name}</div>
              <div className="user-email"><span>Email </span>{user.email}</div>
              <div className="user-status"><span>Status </span>{user.deactivated ? "Inactive" : "Active"}</div>
              <div className="user-zip"><span>Zip code </span>{user.zip_code}</div>
              
              {answerCount}
            </div>
            <div className="button-wrapper">
            <div className={"user-type " + user.type}>{user.type}<div className="lastlogin"> Last Login  {lastLogin ? lastLogin : "Never"}</div></div>
            
            <Button size="sm" block outline color="secondary" type='button' disabled={resettingPassword} onClick={() => this.resetPassword(user.email)}>Email Password Reset</Button>
            <Button size="sm" block outline color="secondary" type='button' disabled={resendingConfirmation} onClick={() => this.resendConfirmationEmail(user.email)}>Resend Confirmation Email</Button>
            <Button size="sm" block outline color="danger"  type='button' onClick={() => this.updateUserActivation(user.id, !user.deactivated)}>{user.deactivated ? 'Re-Activate' : 'Deactivate'}</Button>
          
            </div>
          </div>
        </div>
      )
    });
    return (
      <div>
        {usersList}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    UsersList: {
      result: state.AdminUsersGet.result,
      loading: state.AdminUsersGet.loading,
      error: state.AdminUsersGet.error
    },
    PasswordReset: {
      result: state.ResetPasswordGet.result,
      loading: state.ResetPasswordGet.loading,
      error: state.ResetPasswordGet.error
    },
    ReactivateUser: {
      result: state.AdminUsersReactivate.result,
      loading: state.AdminUsersReactivate.loading,
      error: state.AdminUsersReactivate.error
    },
    DeactivateUser: {
      result: state.AdminUsersDeactivate.result,
      loading: state.AdminUsersDeactivate.loading,
      error: state.AdminUsersDeactivate.error
    },
    ResendConfirmation: {
      result: state.ResendConfirmation.result,
      loading: state.ResendConfirmation.loading,
      error: state.ResendConfirmation.error
    }
  }
}

export default connect(mapStateToProps)(UsersList);