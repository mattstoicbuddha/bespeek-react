import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getQuestionCategoriesList} from "../../../../actions/Admin/Categories/get";
import CategoriesEditor from "../CategoriesEditor";
import helpers from '../../../../helpers';
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCategoryModal: false,
      openCid: null,
      isNewCategory: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
  }

  getCategoriesDispatch() {
    this.props.dispatch(getQuestionCategoriesList());
  }

  componentDidMount(){
    this.getCategoriesDispatch();
  }

  componentDidUpdate(prevProps){
  }

  createNewCategory(e) {
    // Note that `e` here is the click event; we use this to
    // stop the window from scrolling when we open the category editor modal
    e.preventDefault();
    this.setState({showCategoryModal: true, openCid: null, isNewCategory: true});
  }

  openCategory(e, cid) {
    // Note that `e` here is the click event; we use this to
    // stop the window from scrolling when we open the category editor modal
    e.preventDefault();
    this.setState({showCategoryModal: true, openCid: cid, isNewCategory: false});
  }

  closeCategory() {
    this.setState({showCategoryModal: false, openCid: null, isNewCategory: false});
    // Reload our category list in case something changed
    this.props.dispatch(getQuestionCategoriesList());
  }

  render() {
    const categories = this.props.AdminQuestionsCategoriesGet;
    const {showCategoryModal, openCid, isNewCategory} = this.state;
    let categoriesError = "";
    // Ensure we have categories in the first place, or have tried to get them
    if (categories.loading) return (<div></div>);
    // If there was an error loading categories, we need to display that so
    // an admin can look into it
    if (categories.error) {
      categoriesError = (
        <div className="error">
          {categories.error}
        </div>
      );
    }
    const newCategoryLink = (
      <div className="new-category-link" key={"category_new"}>
        <a href='#' onClick={e => this.createNewCategory(e)}>
          Create New Category
        </a>
      </div>
    );
    const categoriesList = [];
    if (categories.result && categories.result.length) {
      categories.result.forEach((category, i) => {
        categoriesList.push((
          <div key={"category_" + category.id}>
            <a href='#' onClick={e => this.openCategory(e, category.id)}>
              {i+1}. {category.category_name}
            </a>
          </div>
        ))
      });
    }
    return (
      <div className="admin-category-list-container">
        <div className="admin-category-list">
          {newCategoryLink}
          {categoriesList}
        </div>
        <CategoriesEditor show={showCategoryModal} cid={openCid} cList={categories.result} isNew={isNewCategory} closeParent={this.closeCategory.bind(this)} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
	return {
    AdminQuestionsCategoriesGet: {
      result: state.AdminQuestionsCategoriesGet.result,
      loading: state.AdminQuestionsCategoriesGet.loading,
      error: state.AdminQuestionsCategoriesGet.error
    }
	};
}

export default connect(mapStateToProps)(Categories);