import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {saveCategory} from "../../../../actions/Admin/Categories/save";
import helpers from '../../../../helpers';
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class CategoryEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: {
        category_description: "",
        category_name: "",
        category_id: 0,
      },
      categoryDropdownOpen: false,
      formError: "",
      formSuccess: ""
    };
    // For clearing categories later
    this.blankCategory = {
        category: {
        category_description: "",
        category_name: "",
        category_id: 0,
      }
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    // Update our formError to clear out when we change
    // an input value, so we aren't taking up a buttload
    // of form space with an error if somebody is trying
    // to fix it
    this.setState({formError: ""});
    // Process our change as normal
    const target = event.target;
    let name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    if (name.indexOf("category.") > -1) {
      // If this is one of the category inputs, we need to clone the category object
      // from the state and assign the new value to the clone, in order to ensure
      // we can set the value of the category obj inside of the state)
      name = name.split(".")[1];
      this.setState({category: Object.assign(this.state.category, {[name]: value})});
    } else {
      this.setState({[name]: value});
    }
  }

  saveCategoryDispatch() {
    const {category} = this.state;
    // Make sure we have the basics
    if (!category.category_name) {
      this.setState({formError: "You did not submit a category name."})
      return;
    }
    if (!category.category_description) {
      this.setState({formError: "You did not submit a category description."})
      return;
    }
    const categoryObj = {
      category_name: category.category_name,
      category_description: category.category_description,
      deleted: category.deleted || 0,
    }
    // To avoid possible issues later in case it is forgotten,
    // we are only passing the props we need based on whether
    // or not this is a new category; if we pass everything,
    // even though we manipulate the data in the reducer, this
    // may change, so we want to avoid running into data problems
    if (this.props.cid) {
      categoryObj.categoryId = this.props.cid;
    }
    // We should have answer text regardless of the type of
    // category, so we don't need to evaluate type here
    this.props.dispatch(saveCategory(categoryObj));
  }

  componentDidMount() {
    const {cid, cList, isNew} = this.props;
    if (cid && !isNew) {
      const category = helpers.getCategoryFromList(cList, cid);
      this.setState({category});
    }
  }

  componentDidUpdate(prevProps){
    const {cid, cList, isNew, AdminQuestionsCategoriesSave} = this.props;
    const catSavePrev = prevProps.AdminQuestionsCategoriesSave;
    const catSave = AdminQuestionsCategoriesSave;
    const cidPrev = prevProps.cid;
    if (isNew && this.state.openCid) {
      this.setState({openCid: null});
      this.closeCategory();
    } else if (cid !== cidPrev) {
      // Get our category from the category list if we have a new one
      const category = helpers.getCategoryFromList(cList, cid);
      this.setState({category});
    }
    if (catSavePrev.loading && !catSave.loading) {
      // We were saving a category before the last update, so we can now
      // check our result and update the state accordingly
      if (!catSave.error) {
        this.setState({formSuccess: "Category successfully saved!"});
        setTimeout(() => {
          this.closeCategory();
        }, 1500);
      } else {
        const error = catSave.error || catSave.result.error;
        this.setState({formError: "There was an issue saving your category: " + error});
      }
    }
  }

  clearCategory() {
    const blank = Object.assign({}, this.blankCategory);
    this.setState({category: blank.category});
  }

  closeCategory() {
    this.setState({show: false, openQid: null});
    this.clearCategory();
    if (helpers.isFunction(this.props.closeParent)) {
      this.props.closeParent();
    }
  }

  render() {
    const {show, cid, cList, isNew, AdminQuestionsSave} = this.props;
    const {category, formError, formSuccess} = this.state;
    // Save us some space by shrinking this down a bit
    const formErrorContainer = [];
    const categoryOpts = [];

    if (!show || !isNew && !cid) return (<div></div>);

    if (formError.length) {
      formErrorContainer.push((
        <div key="formError" className="alert alert-danger">{formError}</div>
      ));
    }

    if (!formError.length && formSuccess.length) {
      formErrorContainer.push((
        <div key="formSuccess" className="alert alert-success">Category saved!</div>
      ));
    }

    return (
      <div className="admin-category-editor">
        {formErrorContainer}
        <Input className="category-text" name="category.category_name" value={category.category_name || ""} onChange={this.handleChange.bind(this)} />
        <textarea className="category-description" name="category.category_description" value={category.category_description || ""} onChange={this.handleChange.bind(this)}></textarea>

       <Button className="use-image submit" onClick={this.saveCategoryDispatch.bind(this)}></Button>
       <Button className="use-image close-modal" onClick={this.closeCategory.bind(this)}></Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    AdminQuestionsCategoriesSave: {
      result: state.AdminQuestionsCategoriesSave.result,
      loading: state.AdminQuestionsCategoriesSave.loading,
      error: state.AdminQuestionsCategoriesSave.error
    }
  };
}

export default connect(mapStateToProps)(CategoryEditor);