import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {getQuestionCategoriesList} from "../../../../actions/Admin/Categories/get";
import {saveQuestion} from "../../../../actions/Admin/Questions/save";
import helpers from '../../../../helpers';
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class QuestionEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qid: null,
      question: {
        text: "",
        category_description: "",
        category_name: "",
        type: "text", // Initial type so we don't end up empty when creating a new question
        category_id: 0,
        deleted: 0,
        true_cat_unlock: 0,
        false_cat_unlock: 0,
        possible_answers: []
      },
      categoryDropdownOpen: false,
      formError: "",
      formSuccess: ""
    };
    // For clearing questions later
    this.blankQuestion = {
      question: {
        text: "",
        category_description: "",
        category_name: "",
        type: "",
        category_id: 0,
        deleted: 0,
        true_cat_unlock: 0,
        false_cat_unlock: 0,
        possible_answers: []
      }
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    // Update our formError to clear out when we change
    // an input value, so we aren't taking up a buttload
    // of form space with an error if somebody is trying
    // to fix it
    this.setState({formError: ""});
    // Process our change as normal
    const target = event.target;
    let name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    if (name.indexOf("question.") > -1) {
      // If this is one of the question inputs, we need to clone the question object
      // from the state and assign the new value to the clone, in order to ensure
      // we can set the value of the queston obj inside of the state)
      name = name.split(".")[1];
      if (name.indexOf("possible_answer_") > -1) {
        // This is part of the possible answers array and needs to be formatted differently
        const possibleAnswersArray = this.state.question.possible_answers;
        // Since we split up indexes by underscores in the name, grabbing the last value
        // should get us an index
        const nameSplit = name.split("_");
        const index = nameSplit.pop();
        const nameInd = nameSplit.join("_").replace("possible_answer_", "");
        possibleAnswersArray[index][nameInd] = value;
        const newQuestionObj = this.state.question;
        newQuestionObj.possible_answers = possibleAnswersArray;
        // We have to do some special manipulation to the state object because we are
        // dealing with the array values that need to be edited individually
        this.setState({question: Object.assign(this.state.question, newQuestionObj)});
        return;
      }
      this.setState({question: Object.assign(this.state.question, {[name]: value})});
    } else {
      this.setState({[name]: value});
    }
  }

  saveQuestionDispatch() {
    const {question} = this.state;
    // Make sure we have the basics
    if (!question.text) {
      this.setState({formError: "You did not submit question text."})
      return;
    }
    if (!question.category_id) {
      this.setState({formError: "You did not select a category."})
      return;
    }
    const questionObj = {
      text: question.text,
      categoryId: question.category_id,
      deleted: question.deleted || 0,
    }
    // To avoid possible issues later in case it is forgotten,
    // we are only passing the props we need based on whether
    // or not this is a new question; if we pass everything,
    // even though we manipulate the data in the reducer, this
    // may change, so we want to avoid running into data problems
    if (this.props.qid) {
      questionObj.questionId = this.props.qid;
    }
    questionObj.type = question.type;
    // Set up our categories for boolean question types
    if (question.type === "boolean") {
      questionObj.true_cat_id = question.true_cat_unlock;
      questionObj.false_cat_id = question.false_cat_unlock;
    }
    // Set up our categories for multiple_choice question types
    if (question.type === "multiple_choice") {
      questionObj.possible_answers = question.possible_answers;
    }
    // We should have answer text regardless of the type of
    // question, so we don't need to evaluate type here
    this.props.dispatch(saveQuestion(questionObj));
  }

  componentDidMount() {
    this.props.dispatch(getQuestionCategoriesList());
  }

  componentDidUpdate(prevProps){
    const oldQid = this.state.qid;
    const newQid = this.props.qid;
    const {qList, isNew, AdminQuestionsSave} = this.props;
    const AdminQuestionsSavePrev = prevProps.AdminQuestionsSave;
    if (oldQid || newQid) {
      if (isNew && oldQid) {
        console.log("isnew");
        this.clearQuestion();
      } else if (!isNew && !oldQid) {
        // Get our question from the question list if we have a new one
        const question = helpers.getQuestionFromList(qList, newQid);
        this.setState({question, qid: newQid});
      } else if (oldQid && newQid !== oldQid) {
        // Get our question from the question list if we have a new one
        const question = helpers.getQuestionFromList(qList, newQid);
        this.setState({question, qid: newQid});
      }
    }
    if (AdminQuestionsSavePrev.loading && !AdminQuestionsSave.loading) {
      // We were saving a question before the last update, so we can now
      // check our result and update the state accordingly
      if (!AdminQuestionsSave.error) {
        this.setState({formSuccess: "Question successfully saved!"});
        setTimeout(() => {
          this.closeQuestion();
        }, 1500);
      } else {
        const error = AdminQuestionsSave.error || AdminQuestionsSave.result.error;
        this.setState({formError: "There was an issue saving your question: " + error});
      }
    }
  }

  clearQuestion() {
    const blank = Object.assign({}, this.blankQuestion);
    this.setState({question: blank.question, qid: null});
  }

  closeQuestion() {
    this.setState({show: false, openQid: null});
    this.clearQuestion();
    if (helpers.isFunction(this.props.closeParent)) {
      this.props.closeParent();
    }
  }

  addMultipleChoiceField() {
    const blankAnswer = {
      cat_unlock: 0,
      answer_text: "",
      answer_weight: 0
    }
    const questionObj = this.state.question;
    questionObj.possible_answers.push(blankAnswer);
    this.setState({question: questionObj});
  }

  deleteMultipleChoiceField(ind) {
    const questionObj = this.state.question;
    questionObj.possible_answers.splice(ind, 1);
    this.setState({question: questionObj});
  }

  toggleCategoryDropdown() {
    this.setState({categoryDropdownOpen: !this.state.categoryDropdownOpen});
  }

  render() {
    const {show, qid, qList, isNew, AdminQuestionsCategoriesGet, AdminQuestionsSave} = this.props;
    const {question, formError, formSuccess} = this.state;
    const formErrorContainer = [];
    const categoryOpts = [];
    // Set this later to display the currently-selected category description
    let selectedCategoryDescription = "";
    // Do nothing if we aren't supposed to show or don't have a question id
    if (!show) return (<div></div>);
    const questionTypeDisplay = [];
    // If we have a question type and this isn't a new question,
    // we need to display the type but not an input, since we
    // don't allow type changes after question creation
    if (question.type && question.type.length && !isNew) {
      questionTypeDisplay.push((
        <div key="category-type-div" className="category-type-div"><h3>{helpers.capitalizeFirstLetter(question.type.replace(/_/g, " "))}</h3></div>
      ));
    } else {
      questionTypeDisplay.push((
        <Input key="category-type-input" type="select" className="category-type-input" name="question.type" value={question.type || ""} onChange={this.handleChange.bind(this)}>
          <option value="text">Text</option>
          <option value="boolean">Boolean (yes/no)</option>
          <option value="multiple_choice">Multiple Choice</option>
        </Input>
      ));
    }
    if (AdminQuestionsCategoriesGet.result) {
      AdminQuestionsCategoriesGet.result.forEach(category => {
        // We are doing two things here:

        // 1. Getting our options set up for selecting a category
        categoryOpts.push((<option key={"category_" + category.id} value={category.id}>{category.category_name}</option>));
        // 2. Getting a description for the category that is currently displayed;
        // we do this in this loop because there is no need to do *another* loop
        // to get the same info
        if (category.id === question.category_id) {
          selectedCategoryDescription = category.category_description;
        }
      });
    }

    // We need to set up the appropriate fields for our question types:
    // Text: no additional fields
    // Boolean: Category unlock for true and/or false
    // Multiple Choice: input text boxes
    const additionalFields = [];
    if (question.type) {
      if (question.type === "boolean") {
        additionalFields.push((
          <div key="true-false-fields">
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                <label className="input-group-text" htmlFor="question-true-category-select">True</label>
                </div>
            <Input type="select" name="question.true_cat_unlock" id="question-true-category-select" value={question.true_cat_unlock || ""} onChange={this.handleChange.bind(this)}>
              
              
              <option key={"category_none"} value="0">None</option>
              {categoryOpts}
            </Input>
            </div>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                <label className="input-group-text" htmlFor="question-false-category-select">False</label>
                </div>
            <Input type="select" name="question.false_cat_unlock" id="question-false-category-select" value={question.false_cat_unlock || ""} onChange={this.handleChange.bind(this)}>
              
              <option key={"category_none"} value="0">None</option>
              {categoryOpts}
            </Input>
            </div>
          </div>
        ));
      } else if (question.type === "multiple_choice") {
        const multipleChoiceFields = [];
        if (question.possible_answers && question.possible_answers.length) {
          // Set up an index so we know what fields go where when we save this
          let ind = 0;
          question.possible_answers.forEach((answer, i) => {
            ind = i;
            multipleChoiceFields.push((
              <div className="possible-answers" key={'possible-answers-' + ind}>

              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <label className="input-group-text" htmlFor="inputGroupSelect01">Answer</label>
                </div>
                <Input id="inputGroupSelect01" value={answer.answer_text} name={"question.possible_answer_answer_text_" + i} onChange={this.handleChange.bind(this)} />
              </div>

                <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <label className="input-group-text" htmlFor="inputGroupSelect02">Category Unlock</label>
                </div>
                <Input id="inputGroupSelect02" type="select" name={"question.possible_answer_cat_unlock_" + i} className="question-possible-answer-category-select" value={answer.cat_unlock || ""} onChange={this.handleChange.bind(this)}>
                  <option key={"category_none"} value="0">None</option>
                  {categoryOpts}
                </Input>
                </div>

                <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <label className="input-group-text" htmlFor="inputGroupSelect03">Answer Weight</label>
                </div>
                <Input id="inputGroupSelect03" name={"question.possible_answer_answer_weight_" + i} className="question-possible-answer-weight" value={answer.answer_weight || 0} onChange={this.handleChange.bind(this)} />
                </div>
                <Button onClick={this.deleteMultipleChoiceField.bind(this, ind)}>Delete Field</Button>
              </div>
            ));
          });
        }
        additionalFields.push((
          <div key="multiple-choice-fields">
            {multipleChoiceFields}
            <Button onClick={this.addMultipleChoiceField.bind(this)}>Add Field</Button>
          </div>
        ));
      }
    }

    if (formError.length) {
      formErrorContainer.push((
        <div key="formError" className="alert alert-danger">{formError}</div>
      ));
    }

    if (!formError.length && formSuccess.length) {
      formErrorContainer.push((
        <div key="formSuccess" className="alert alert-success">Question saved!</div>
      ));
    }

    return (
      <div className="admin-question-editor">
        {formErrorContainer}
        <div className="input-group mb-3">
         <div className="input-group-prepend">
            <label className="input-group-text" htmlFor="inputGroupSelect05">Question</label>
          </div>
        <Input id="inputGroupSelect05" className="question-text" name="question.text" value={question.text || ""} onChange={this.handleChange.bind(this)} />
        </div>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
          <Label className="input-group-text" htmlFor="category-select">Category</Label>
          </div>
          <Input type="select" name="question.category_id" id="category-select" value={question.question_category_id || question.category_id || ""} onChange={this.handleChange.bind(this)}>
            <option key={"category_none"} value="">Select</option>
            {categoryOpts}
          </Input>
        </div>
        <div className='selected-category-description'>
          {selectedCategoryDescription}
        </div>
        {questionTypeDisplay}
        {additionalFields}
       <Button className="use-image submit" onClick={this.saveQuestionDispatch.bind(this)}></Button>
       <Button className="use-image close-modal" onClick={this.closeQuestion.bind(this)}></Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    AdminQuestionsCategoriesGet: {
      result: state.AdminQuestionsCategoriesGet.result,
      loading: state.AdminQuestionsCategoriesGet.loading,
      error: state.AdminQuestionsCategoriesGet.error
    },
    AdminQuestionsSave: {
      result: state.AdminQuestionsSave.result,
      loading: state.AdminQuestionsSave.loading,
      error: state.AdminQuestionsSave.error
    }
  };
}

export default connect(mapStateToProps)(QuestionEditor);