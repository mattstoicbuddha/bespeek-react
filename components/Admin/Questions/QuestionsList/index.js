import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getQuestionList} from "../../../../actions/Admin/Questions/get";
import QuestionsEditor from "../QuestionsEditor";
import helpers from '../../../../helpers';
import cookieHelper from '../../../../helpers/cookies';
import './style.scss';

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNewQuestion: false,
      AdminQuestionsGet: {},
      showQuestionModal: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = !isNaN(target.value) ? parseInt(target.value) : target.value;
    this.setState({[name]: value});
  }

  getQuestionsDispatch() {
    this.props.dispatch(getQuestionList());
  }

  componentDidMount(){
    this.getQuestionsDispatch();
  }

  componentDidUpdate(prevProps){
  }

  createNewQuestion(e) {
    // Note that `e` here is the click event; we use this to
    // stop the window from scrolling when we open the question editor modal
    e.preventDefault();
    this.setState({showQuestionModal: true, openQid: null, isNewQuestion: true});
  }

  openQuestion(e, qid) {
    // Note that `e` here is the click event; we use this to
    // stop the window from scrolling when we open the question editor modal
    e.preventDefault();
    this.setState({showQuestionModal: true, openQid: qid, isNewQuestion: false});
  }

  closeQuestion() {
    this.setState({showQuestionModal: false, openQid: null, isNewQuestion: false});
    // Reload our question list in case something changed
    this.props.dispatch(getQuestionList());
  }

  render() {
    const questions = this.props.AdminQuestionsGet;
    const {showQuestionModal, openQid, isNewQuestion} = this.state;
    let questionsError = "";
    // Ensure we have questions in the first place, or have tried to get them
    if (questions.loading) return (<div></div>);
    // If there was an error loading questions, we need to display that so
    // an admin can look into it
    if (questions.error) {
      questionsError = (
        <div className="error">
          {questions.error}
        </div>
      );
    }
    const newQuestionLink = (
      <div className="new-question-link" key={"question_new"}>
        <a href='#' onClick={e => this.createNewQuestion(e)}>
          Create New Question
        </a>
      </div>
    );
    const questionsList = [];
    if (questions.result && questions.result.length) {
      questions.result.forEach((question, i) => {
        questionsList.push((
          <div key={"question_" + question.id}>
            <a href='#' onClick={e => this.openQuestion(e, question.id)}>
              {i+1}. {question.text} ({helpers.capitalizeFirstLetter(question.type.replace(/_/g, " "))})
            </a>
          </div>
        ))
      });
    }
    return (
      <div className="admin-question-list-container  bg-white">
        <div className="admin-question-list">
          {newQuestionLink}
          {questionsList}
        </div>
        <QuestionsEditor show={showQuestionModal} qid={openQid} qList={questions.result} isNew={isNewQuestion} closeParent={this.closeQuestion.bind(this)} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
	return {
    AdminQuestionsGet: {
      result: state.AdminQuestionsGet.result,
      loading: state.AdminQuestionsGet.loading,
      error: state.AdminQuestionsGet.error
    }
	};
}

export default connect(mapStateToProps)(Questions);