import React, { Component } from 'react';
import {connect} from "react-redux";
import Head from 'next/head'
import Link from 'next/link'
import cookieHelper from '../../helpers/cookies';
import HeaderComponent from '../HeaderComponent'
import './bespeekstrap.scss';
import './global.scss';

class Layout extends Component {
    constructor(props) {
        super(props);
        this.children = props.children;
        this.title = props.title;
        this.pageClass = "page-" + (props.pageClass ? props.pageClass : this.slugify(this.title));
    }

    slugify(string) {
      const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
      const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
      const p = new RegExp(a.split('').join('|'), 'g')

      return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
    }

    render() {
        return (
            <div className={"mainwrapper " + this.pageClass}>
                <Head>
                    <title>{ this.title }</title>
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='initial-scale=1.0, width=device-width' />
                    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png" />
                    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png" />
                    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png" />
                    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png" />
                    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png" />
                    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png" />
                    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png" />
                    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png" />
                    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png" />
                    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png" />
                    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png" />
                    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                    <link rel="manifest" href="/manifest.json" />
                    <meta name="msapplication-TileColor" content="#ffffff" />
                    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
                    <meta name="theme-color" content="#ffffff" />
                </Head>

                <div className={"app content-main"}>
                    <div className="flex-header">
                        <HeaderComponent />
                        <div className="branding-container">
                            <img className="btn-home-hero-logo" src="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/bee-white.png"/>
                            <h1 className="pagetitle">{ this.title }</h1>
                        </div>
                    </div>
                    <div className="container">
                        <div className="content-children">
                            { this.children }
                        </div>

                    </div>
                </div>
                <footer>
                    <div className="footer-wraper">
                        <div className="container">
                            <p className="copyrightFooter">Copyright 2020 Bespeek</p>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {};
}

export default connect(mapStateToProps)(Layout);
