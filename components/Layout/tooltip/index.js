import React, { Component } from 'react';
import {connect} from "react-redux";
import Router from 'next/router';
import { Tooltip } from 'reactstrap';
import config from '../../../env';
import './style.scss';

class BespeekTooltip extends Component {
  constructor(props) {
    super(props);
    this.state = {
    	open: false
    }
  }

  toggle() {
  	this.setState({open: !this.state.open});
  }

  render() {
  	const {target, placement, children} = this.props;
    let className = 'bespeek-tooltip-holder';
    if (this.props.className) {
      className = this.props.className;
    }
  	return (
	  	<Tooltip className={className} placement={placement} isOpen={this.state.open} target={target} toggle={this.toggle.bind(this)}>
	      {children}
	    </Tooltip>
	);
  }
}

export default BespeekTooltip;