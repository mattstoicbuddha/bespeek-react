import React, { Component } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import cookieHelper from '../../helpers/cookies';
import {Overlay as QuestionOverlay} from '../Users/Question';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import OffCanvasNavigation from '../OffCanvasNavigation';
import UserInfo from '../Users/Profile/UserInfo';
import './style.scss';




const Navigation = () => {
  let adminDashboardLink = "";
  const role = cookieHelper.getRole();
  console.log({role})
  if (role === 1) {
    adminDashboardLink = (<li>
       <Link href={{ pathname: 'admin/' }} prefetch><a>Admin Dashboard</a></Link>
    </li>)
  }
  let partnerDashboardLink = "";
  if (cookieHelper.isSponsor()) partnerDashboardLink = (
    <li>
       <Link href={{ pathname: '/sponsors/dashboard' }} prefetch><a>Partner Dashboard</a></Link>
    </li>
  );
  return (
    <nav id="menu">
      <ul>
         <li>
            <Link href={{ pathname: '/' }} prefetch><a>Home</a></Link>
          </li>
          <li>
             <Link href={{ pathname: '/about' }} prefetch><a>About</a></Link>
          </li>
          <li>
             <Link href={{ pathname: '/users/register' }} prefetch><a>Register</a></Link>
          </li>
          <li>
             <Link href={{ pathname: '/users/login' }} prefetch><a>Log In</a></Link>
          </li>
          {adminDashboardLink}
          {partnerDashboardLink}
      </ul>
    </nav>
  )
};

class HeaderNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      showMenu: false,
      showQuestionOverlay: false
    };
  }

  componentDidMount() {
    if (cookieHelper.getTokenCookie()) this.setState({"loggedIn": true});
  }

  logout() {
    cookieHelper.clearTokenCookie();
    this.setState({"loggedIn": false});
    Router.push("/");
  }

  toggleMenu() {
    console.log('should toggle to ' + !this.state.showMenu);
    this.setState({"showMenu": !this.state.showMenu});
  }

  toggleQuestionOverlay() {
    this.setState({"showQuestionOverlay": !this.state.showQuestionOverlay});
  }

  render() {
    const {showMenu, showQuestionOverlay} = this.state;

    const links = [
      (
      <li key="home">
        <Link href={{ pathname: '/' }}>
            <a>Home</a>
          </Link>
        </li>
      ),
      (
      <li key="about">
        <Link href={{ pathname: '/about' }}>
            <a>About</a>
          </Link>
        </li>
        )
    ];
  
    if (!this.state.loggedIn) {
      links.push(
        (
          <li key="register">
            <Link prefetch href={{ pathname: '/users/register' }}>
                <a>Register</a>
              </Link>
            </li>
          ),
          (
            <li key="login">
              <Link prefetch href={{ pathname: '/users/login' }}>
                <a>Log In</a>
              </Link>
            </li>
          )
      )
    } else {
      const role = cookieHelper.getRole();
      let adminMenu = "";
      if(role === 1) {
        adminMenu = (
        <li key="qedit">
          <Link href={{ pathname: '/admin/' }}>
              <a>Admin Menu</a>
            </Link>
          </li>
        );
      }
      let profileLink = (
        <li key="profile">
            <Link prefetch href={{ pathname: '/users/profile' }}>
              <a>Profile</a>
            </Link>
          </li>
      );
      let qOverlay = (
        <li key='question-overlay'>
          <a href="#" onClick={this.toggleQuestionOverlay.bind(this)}>New Question</a>
        </li>
      );
      if (cookieHelper.isSponsor()) {
        qOverlay = "";
        profileLink = (
          <li key="dashboard">
              <Link prefetch href={{ pathname: '/sponsors/dashboard' }}>
                <a>Dashboard</a>
              </Link>
            </li>
        );
      }

      links.push(
        adminMenu,
        qOverlay,
        profileLink,
        (
          <li key="logout">
            <a href="#" onClick={this.logout.bind(this)}>Log Out</a>
          </li>
        )
      )
    }
    let profileWidget = "";
    let questionOverlay = "";
    if (this.state.loggedIn && !cookieHelper.isSponsor()) {
      profileWidget = (<UserInfo/>);
      questionOverlay = (<QuestionOverlay render="none" show={showQuestionOverlay} closeParent={this.toggleQuestionOverlay.bind(this)} />);
    }
    return (
      <div id="collapse-main-nv">
          <a className="collapse-main-nv" href="#" onClick={this.toggleMenu.bind(this)}><img src="https://d12hpcyicmxkwr.cloudfront.net/menu2-white.png"/></a>
        <OffCanvasNavigation isOpen={this.state.showMenu} onClose={this.toggleMenu.bind(this)}>
           <ul className="main-nv">
              {links}
              <li>
              <a href="#" onClick={this.toggleMenu.bind(this)}>Close</a>
            </li>
           </ul>
           <div className="off-canvas-extra">
              {profileWidget}
           </div>
            {questionOverlay}
        </OffCanvasNavigation>

      </div>
    );
  }
}

export default HeaderNavigation;
