import Dashboard from "./Dashboard";
import Registration from "./Registration";
import Onboarding from "./Onboarding";
export {
	Dashboard,
	Registration,
	Onboarding
}