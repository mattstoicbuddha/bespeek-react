import React, { Component } from 'react';
import {connect} from "react-redux";
import {registerUser} from "../../../actions/Users/register";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import validateRegistration from "../../../validation/Users/register";
import analyticsHelper from '../../../helpers/analytics';
import './style.scss';

class SponsorOnboarding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMoreInfo: false,
      showMoreInfoOpen: false,
      firstQuartile: false,
      secondQuartile: false,
      thirdQuartile: false,
      videoEnded: false
    }
  }

  timeUpdate(e) {
  	const event = e.nativeEvent;
  	const elem = event.srcElement;
  	const duration = Math.round(elem.duration);
	const time = Math.round(elem.currentTime);
	const percentage = (time / duration) * 100;
	const {firstQuartile, secondQuartile, thirdQuartile} = this.state;
	if (percentage >= 25 && !firstQuartile) {
		this.an('video-first-quartile');
		this.setState({firstQuartile: true})
	} else if (percentage >= 50 && !secondQuartile) {
		this.an('video-second-quartile');
		this.setState({secondQuartile: true})
	} else if (percentage >= 75 && !thirdQuartile) {
		this.an('video-third-quartile');
		this.setState({thirdQuartile: true})
	}
  }

  an(ev) {
  	if (ev === 'video-ended') {
  		this.setState({videoEnded: true})
  	}
    analyticsHelper.send(ev, "sponsor-onboarding");
  }

  toggle() {
  	this.setState({showMoreInfo: !this.state.showMoreInfo});
	const {showMoreInfoOpen} = this.state;
	const showMoreInfo = !this.state.showMoreInfo; // to match the state change
	if (showMoreInfo && !showMoreInfoOpen) {
		this.setState({showMoreInfoOpen: true});
		this.an('show-more-toggle');
	}
  }
  render() {
  	const {showMoreInfo} = this.state;
	return (
		<div className='onboarding'>
			<div className="vid-container">
				<video controls poster="https://d12hpcyicmxkwr.cloudfront.net/bespeek+partner+video+cover.png"
					onLoadedData={this.an.bind(this, 'video-loaded')}
					onError={this.an.bind(this, 'video-error')}
					onPlay={this.an.bind(this, 'video-play')}
					onPause={this.an.bind(this, 'video-pause')}
					onTimeUpdate={this.timeUpdate.bind(this)}
					onEnded={this.an.bind(this, 'video-ended')}
				>
				    <source src="https://d12hpcyicmxkwr.cloudfront.net/bespeek+partner+video+2020.mp4"
				        type="video/mp4" />
					Sorry, your browser doesn't support embedded videos.
				</video>	
			</div>
			
			<div className="show">
				<div>
					<h2>What is Bespeek?</h2>
					<p>Bespeek is a revolutionary online platform to get people pre-planning.  Bespeek provides users with questions via a simple guided process. This allows users to get their final arrangements in place in small manageable sections. Users can answer many questions or just a few at a time until their profile is complete. Of course, users can edit their responses at any time to keep their profile current and up to date. In addition to managing a final arrangements profile, a user can add “connections” to other people who they trust with their final wishes. After death, the people set as “connections” will have view access to an individual's Bespeek final wishes profile. </p>
						<p><strong>Thanks to the Bespeek Partner acting as a sponsor, individuals may have thier Bespeek membership for life. </strong></p>
					<div className='onboarding'>
						<div className="vid-container">
							<video controls poster="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/bespeek+user+dashboard+cover.jpg"
								onLoadedData={this.an.bind(this, 'video-loaded')}
								onError={this.an.bind(this, 'video-error')}
								onPlay={this.an.bind(this, 'video-play')}
								onPause={this.an.bind(this, 'video-pause')}
								onTimeUpdate={this.timeUpdate.bind(this)}
								onEnded={this.an.bind(this, 'video-ended')}
							>
							    <source src="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/bespeek+user+dashboard.mp4"
							        type="video/mp4" />
								Sorry, your browser doesn't support embedded videos.
							</video>	
						</div>
					</div>

					<h2>What is a Bespeek Partner?</h2>
						<ul>
							<li>Bespeek Partners are eligible Funeral Homes and Mortuary Services who sponsor individuals memberships to Bespeek.</li>
							<li>As a Bespeek Partner you can offer the Bespeek membership registration right on your website with the easy to add Bespeek website widget.</li>
							<li>The grateful Bespeek members you sponsor will be available via your Bespeek Partner dashboard so you may reach out to them to support their pre-need planning.</li>
						</ul>
					<div className='onboarding'>
						<div className="vid-container">
							<video controls poster="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/bespeek+partner+dashboard+cover.jpg"
								onLoadedData={this.an.bind(this, 'video-loaded')}
								onError={this.an.bind(this, 'video-error')}
								onPlay={this.an.bind(this, 'video-play')}
								onPause={this.an.bind(this, 'video-pause')}
								onTimeUpdate={this.timeUpdate.bind(this)}
								onEnded={this.an.bind(this, 'video-ended')}
							>
							    <source src="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/bespeek+partner+dashboard.mp4"
							        type="video/mp4" />
								Sorry, your browser doesn't support embedded videos.
							</video>	
						</div>
					</div>

					<h2>How can Bespeek help my pre-need and overall marketing?</h2>
					<ul>
					    <li>Offer your visitors a valuable service for free.</li>
					    <li>Build strong relationships with your visitors, and their friends & families.</li>
					    <li>Provide conversation starters pre-need primers.</li>
					    <li>Have a constantly self filling pre-need lead funnel.</li>
					    <li>Qualify pre-need prospects who are ready to plan.</li>
					    <li>Grow your prospect base exponentially via user connections.</li>
					    <li>Save time and money on mailers and old fashioned lead generating events.</li>
					    <li>Market with a positive and no-pressure Win/Win approach.</li>
					    <li>Attract more long-term and per-retirement prospects.</li>
					    <li>Collect lifetime growth leads and re-market to them over time.</li>
				    </ul>
				</div>
			</div>
		</div>
	);
  }
}

export default SponsorOnboarding;