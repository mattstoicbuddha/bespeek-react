import config from '../../../../env';
import React, { Component } from 'react';
import {connect} from "react-redux";
import {getEmbedCodeSettings, updateEmbedCodeSettings} from "../../../../actions/Sponsors";
import { SketchPicker } from 'react-color';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './style.scss';

class EmbedCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonBg: "#eb6864",
      buttonQuad: "bottom-left",
      showEmbedCodeArea: false
    };
    this.handleChangeComplete = this.updateColor.bind(this);
  }

  updateColor(color) {
    this.setState({ buttonBg: color.hex });
  }

  updateQuadrant(event) {
    const target = event.target;
    const value = target.value;
    this.setState({ buttonQuad: value });
    console.log({state: this.state})
  }

  saveSettings() {
    const settings = {
      backgroundColor: this.state.buttonBg,
      quadrantClass: this.state.buttonQuad
    }
    this.setState({updateFailed: false});
    this.props.dispatch(updateEmbedCodeSettings(settings));
  }

  componentDidMount() {
    console.log("should dispatch");
    this.props.dispatch(getEmbedCodeSettings());
  }

  componentWillUpdate() {
    const getSettings = this.props.getSettings;
    const updateSettings = this.props.updateSettings;
    if (!getSettings.loading && (getSettings.result || getSettings.error)) {
      if (this.props.onReady && !this.state.onReadyFired) {
        this.setState({onReadyFired: true})
        this.props.onReady();
      }
      if (getSettings.result && getSettings.result.id && !this.state.sponsorId) {
        const {result} = getSettings;
        console.log({result})
        const update = {
          sponsorId: result.id,
        }
        if (result.backgroundColor) {
          update.buttonBg = result.backgroundColor;
        }
        if (result.quadrantClass) {
          update.quadrantClass = result.quadrantClass;
        }
        this.setState(update);
      }
    }
    if (!updateSettings.loading && (updateSettings.result || updateSettings.error)) {
      if (updateSettings.error && !this.state.updateFailed) {
        this.setState({updateFailed: true});
        console.log("Your changes could not be saved.");
        console.log({updateSettingsError: updateSettings.error})
      } else if (updateSettings.result) {
        console.log("Worked, show some proof");
      }
    }
  }

  toggleEmbedCodeArea() {
    this.setState({showEmbedCodeArea: !this.state.showEmbedCodeArea})
  }

  render() {
    const {result, loading, error} = this.props.getSettings;
    const {showEmbedCodeArea} = this.state;
    if (loading) return (<div></div>);
    if (!loading && error) return (<div>{error}</div>);

    let embedCode = "<script src='" + config.api + "/embedcode/js/" + result.id + "'></script>";

    return (
      <div className='embed-code-holder popbox bg-white'>
        <h3 onClick={this.toggleEmbedCodeArea.bind(this)}>Get Embed Code <span className="material-icons">expand_more</span></h3>



        <div className={"embed-section-wrapper" + (showEmbedCodeArea ? " show" : "")}>

          <p className="instructions">Copy the embed code below and paste into your website before the closing body tag.<span className="codesnippet"> {"</body>"}</span></p>
          <div className='embed-code'>
            {embedCode}
          </div>
        <p className="instructions">Select your widget color and location Below and then "SAVE PREFERENCES"</p>
        <div className='button-color flexrow'>
          <div className="color-picker flexthird">
            <h3>Widget Color</h3>
            <SketchPicker
            color={ this.state.buttonBg }
            onChangeComplete={ this.handleChangeComplete } />
          </div>

          <div className='button-show flexthird'>
            <h3>Widget Preview</h3>
            <img src='https://bespeek-react-assets.s3-us-west-2.amazonaws.com/widget-button-transparent2.png' style={{background: this.state.buttonBg}} />
            <div className="widget-location">
              <h3>Widget Location</h3>
              <Input type="select" name="select" onChange={this.updateQuadrant.bind(this)}>
                <option value='top-left' selected={this.state.buttonQuad === 'top-left'}>Top Left</option>
                <option value='top-right' selected={this.state.buttonQuad === 'top-right'}>Top Right</option>
                <option value='bottom-left' selected={this.state.buttonQuad === 'bottom-left'}>Bottom Left</option>
                <option value='bottom-right' selected={this.state.buttonQuad === 'bottom-right'}>Bottom Right</option>
              </Input>
             </div>
            </div>
          </div>


        <div className='embed-code-save'>
          <Button className="btn btn-info" onClick={this.saveSettings.bind(this)}>Save Preferences</Button>
        </div>
      </div>
    </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
      getSettings: {
        result: state.SponsorEmbedCodeSettings.result,
        loading: state.SponsorEmbedCodeSettings.loading,
        error: state.SponsorEmbedCodeSettings.error
      },
      updateSettings: {
        result: state.SponsorEmbedCodeSettingsUpdate.result,
        loading: state.SponsorEmbedCodeSettingsUpdate.loading,
        error: state.SponsorEmbedCodeSettingsUpdate.error
      }
    };
}

export default connect(mapStateToProps)(EmbedCode);