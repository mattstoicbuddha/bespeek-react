import SponsorInfo from "./SponsorInfo";
import EmbedCode from "./EmbedCode";
import Sponsees from "./Sponsees";
export default {
	SponsorInfo,
	EmbedCode,
	Sponsees
}