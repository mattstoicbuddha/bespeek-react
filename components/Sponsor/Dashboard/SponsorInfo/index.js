import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getSponsorProfile, getSponsorProfileClearState} from "../../../../actions/Sponsors/getProfile";
import {updateSponsorProfile} from "../../../../actions/Sponsors/updateProfile";
import './style.scss';

class SponsorInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditor: false,
      savingEdits: false,
      companyName: "",
      contactName: "",
      contactNumber: "",
      companyAddress: "",
      companyCity: "",
      companyZip: "",
      companyLogoUpload: ""
    };
    this.handleChange = this.handleChangeEvent.bind(this);
  }

  async handleChangeEvent(event) {
    // Process our change as normal
    const target = event.target;
    let name = target.name;
    if (name === "companyLogoUpload") {
      const img = await this.convertImgToBase64(target.files[0]);
      this.setState({"companyLogoUpload": img});
      return;
    }
    this.setState({[name]: target.value});
  }

  componentDidMount() {
    this.props.dispatch(getSponsorProfile());
  }

  componentWillUnmount() {
    this.props.dispatch(getSponsorProfileClearState());
  }

  componentWillUpdate() {
    const {result, loading, error} = this.props;
    if (!loading && (result || error)) {
      if (this.props.onReady && !this.state.onReadyFired) {
        this.props.onReady();
        this.setState({onReadyFired: true})
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (!this.state.companyName) {
      const {result, loading, error} = this.props;
      if (prevProps.loading && !loading && result) {
        const {company_name, logo, city, address, zip_code, contact_name, contact_number} = result;
        this.setState({
          companyName: company_name,
          contactName: contact_name,
          contactNumber: contact_number,
          companyAddress: address,
          companyCity: city,
          companyZip: zip_code.toString()
        });
      }
    }
  }

  toggleEditor() {
    this.setState({showEditor: !this.state.showEditor})
  }

  async saveEdits() {
    this.setState({savingEdits: true});
    const edits = {
      company_name: this.state.companyName,
      contact_name: this.state.contactName,
      city: this.state.companyCity,
      address: this.state.companyAddress,
      zip_code: this.state.companyZip,
      contact_number: this.state.contactNumber,
    }
    if (this.state.companyLogoUpload) {
      edits.logo = this.state.companyLogoUpload;
    }
    await this.props.dispatch(updateSponsorProfile(edits));
    this.setState({savingEdits: false});
    this.toggleEditor();
    this.props.dispatch(getSponsorProfile());
  }

  async convertImgToBase64(file) {
    const getBase64Img = new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      if (file) {
        reader.readAsDataURL(file);
      } else {
        return resolve("");
      }
    });
    return await getBase64Img;
  }

  render() {
    const {result, loading, error} = this.props;
    const {showEditor, savingEdits} = this.state;
    if (loading && !result && !error) return (<div></div>);
    if (!loading && error) {
      return (<div>{error}</div>);
    }
    const {company_name, logo, city, address, zip_code, contact_name, contact_number} = result;

    return (
      <div className="sponsor-info-container popbox bg-white">
        <div className={"company-info" + (showEditor ? " hide" : "")}>

          <div className="company-name"><h3>{company_name}</h3></div>
          <div className="company-info-row flexrow">
            <div className="company-info-col">
              <div className="logo"><img src={logo} /></div>
            </div>
            <div className="company-info-col">
              <div className="company-contact-name"><span>Contact:</span> {contact_name}</div>
              <div className="company-number"><span>Phone:</span> {contact_number}</div>
              <div><span>Address:</span></div>
              <div className="company-address">{address}</div>
              <div className="company-city">{city}</div>
              <div className="company-zip">{zip_code}</div>
            </div>
          </div>
          
          <div className="edit-button">
            <button className="company-edit" type="button" onClick={this.toggleEditor.bind(this)} disabled={savingEdits}>Edit</button>
          </div>
        </div>
        <div className={"company-info-editor" + (showEditor ? " show" : "")}>
          <div className="company-name flex"><span>Company Name:</span> <input name='companyName' type='text' value={this.state.companyName} onChange={this.handleChange} /></div>
          <div className="company-contact-name flex"><span>Company Contact Name:</span> <input name='contactName' type='text' value={this.state.contactName} onChange={this.handleChange} /></div>
          <div className="company-number flex"><span>Company Number:</span> <input name='contactNumber' type='text' value={this.state.contactNumber} onChange={this.handleChange} /></div>
          <div className="company-address flex"><span>Company Address:</span> <input name='companyAddress' type='text' value={this.state.companyAddress} onChange={this.handleChange} /></div>
          <div className="company-city flex"><span>Company City:</span> <input name='companyCity' type='text' value={this.state.companyCity} onChange={this.handleChange} /></div>
          <div className="company-zip flex"><span>Company Zip:</span> <input name='companyZip' type='text' value={this.state.companyZip} onChange={this.handleChange} /></div>
          <div className="logo"><img src={this.state.companyLogoUpload} /><input name='companyLogoUpload' type='file' onChange={this.handleChange} /></div>
          <button className="company-save" type="button" onClick={this.saveEdits.bind(this)} disabled={savingEdits}>Save</button>
          <div className="close-button">
            <button  type="button" onClick={this.toggleEditor.bind(this)} disabled={savingEdits}>Close</button>
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
    return {
      result: state.SponsorProfile.result,
      loading: state.SponsorProfile.loading,
      error: state.SponsorProfile.error
    };
}

export default connect(mapStateToProps)(SponsorInfo);