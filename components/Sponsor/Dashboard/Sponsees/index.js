import React, { Component } from 'react';
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {getSponsorProfile, getSponsorProfileClearState} from "../../../../actions/Sponsors/getProfile";
import {updateSponsorProfile} from "../../../../actions/Sponsors/updateProfile";
import './style.scss';

class Sponsees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onReadyFired: false
    }
  }

  formatCreatedAtDate(ts) {
    const d = new Date(ts);
    if (isNaN(d)) {
      // Invalid date
      return "";
    }
    const setPad = v => ("0" + v).slice(-2); 
    return setPad(d.getMonth() + 1) + "-" + setPad(d.getDate()) + "-" + d.getFullYear();
  }

  componentDidUpdate() {
    const {result, loading, error} = this.props;
    if (!loading && (result || error)) {
      if (this.props.onReady && !this.state.onReadyFired) {
        this.props.onReady();
        this.setState({onReadyFired: true})
      }
    }
  }

  render() {
    const {result, loading, error} = this.props;
    if (loading && !result && !error) return (<div></div>);
    if (!loading && error) {
      return (<div>{error}</div>);
    }
    const {sponsees} = result;
    const sponseesContainer = [];

    if (sponsees && sponsees.length) {
      sponsees.sort((a, b) => b.user.created_at - a.user.created_at).forEach((sp, i) => {
        const {first_name, last_name, email, profile_image, created_at, id} = sp.user;
        const container = (
          <div key={'sponsees-' + i} className="sponsee-container">
            <div className="name spcol"><a href={'/connections/profile/' + id}>{first_name} {last_name}</a></div>
            <div className="email spcol">{email}</div>
            <div className="answered spcol">Ans: {sp.answers.length}</div>
            <div className="connections spcol">Con: {sp.connections.length}</div>
            <div className="created_at spcol">{this.formatCreatedAtDate(created_at)}</div>
          </div>
        );
        sponseesContainer.push(container);
      })
    }

    return (<div className="sponsees popbox"><h3>Sponsees</h3>{sponseesContainer}</div>)

  }

}

const mapStateToProps = (state) => {
    return {
      result: state.SponsorProfile.result,
      loading: state.SponsorProfile.loading,
      error: state.SponsorProfile.error
    };
}

export default connect(mapStateToProps)(Sponsees);