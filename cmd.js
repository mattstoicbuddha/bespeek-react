const cmd = require("./commands"),
	  args = require('yargs').argv;

if (Array.isArray(args.command)) {
	console.log("Multiple commands received.");
	return;
}

const command = args.command;

if (command === "component:make") {
	const {name} = args;
	console.log("Creating component: " + name);
	return cmd.makeComponent(name);
}

console.log("Command unrecognized.");

