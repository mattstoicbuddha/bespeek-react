import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_ANSWER_SAVE_BEGIN   = 'FETCH_ANSWER_SAVE_BEGIN';
export const FETCH_ANSWER_SAVE_SUCCESS = 'FETCH_ANSWER_SAVE_SUCCESS';
export const FETCH_ANSWER_SAVE_FAILURE = 'FETCH_ANSWER_SAVE_FAILURE';

export const saveAnswerBegin = () => {
  return ({
    type: FETCH_ANSWER_SAVE_BEGIN
  });
}

export const saveAnswerSuccess = message => ({
  type: FETCH_ANSWER_SAVE_SUCCESS,
  payload: { message }
});

export const saveAnswerFailure = error => ({
  type: FETCH_ANSWER_SAVE_FAILURE,
  payload: { error }
});

export function saveAnswer(opts) {
  const token = cookieHelper.getTokenCookie();
  const body = {
    questionId: opts.questionId,
    answer: "",
    possible_answer_id: null
  }
  // We will have answer text regardless of
  // question type, so check that first
  if (opts.answerText) {
    body.answer = opts.answerText;
  }
  if (!isNaN(opts.answerBoolean)) {
    body.possible_answer_id = opts.answerBoolean;
  } else if (opts.answerMultiple) {
    body.possible_answer_id = opts.answerMultiple;
  }
  return dispatch => {
    dispatch(saveAnswerBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(saveAnswerFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + '/answers/save', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(saveAnswerFailure(json.error));
        } else {
          dispatch(saveAnswerSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(saveAnswerFailure(error)));
  };
}