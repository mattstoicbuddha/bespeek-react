import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_ANSWER_UPDATE_BEGIN   = 'FETCH_ANSWER_UPDATE_BEGIN';
export const FETCH_ANSWER_UPDATE_SUCCESS = 'FETCH_ANSWER_UPDATE_SUCCESS';
export const FETCH_ANSWER_UPDATE_FAILURE = 'FETCH_ANSWER_UPDATE_FAILURE';

export const updateAnswerBegin = () => {
  return ({
    type: FETCH_ANSWER_UPDATE_BEGIN
  });
}

export const updateAnswerSuccess = message => ({
  type: FETCH_ANSWER_UPDATE_SUCCESS,
  payload: { message }
});

export const updateAnswerFailure = error => ({
  type: FETCH_ANSWER_UPDATE_FAILURE,
  payload: { error }
});

export function updateAnswer(opts) {
  const token = cookieHelper.getTokenCookie();
  const body = {
    questionId: opts.questionId,
    answer: "",
    possible_answer_id: 0
  }
  // We will have answer text regardless of
  // question type, so check that first
  if (opts.answerText) {
    body.answer = opts.answerText;
  }
  if (!isNaN(opts.answerBoolean)) {
    body.possible_answer_id = opts.answerBoolean;
  } else if (opts.answerMultiple) {
    body.possible_answer_id = opts.answerMultiple;
  }
  return dispatch => {
    dispatch(updateAnswerBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(updateAnswerFailure("You are not logged in."));
      return {};
    }
    console.log("Fetching...");
    return fetch(config.api + '/answers/update', {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(updateAnswerFailure(json.error));
        } else {
          dispatch(updateAnswerSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(updateAnswerFailure(error)));
  };
}