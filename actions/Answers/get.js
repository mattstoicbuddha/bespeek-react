import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_ANSWERS_GET_BEGIN   = 'FETCH_ANSWERS_GET_BEGIN';
export const FETCH_ANSWERS_GET_SUCCESS = 'FETCH_ANSWERS_GET_SUCCESS';
export const FETCH_ANSWERS_GET_FAILURE = 'FETCH_ANSWERS_GET_FAILURE';

export const getAnswerBegin = () => {
  return ({
    type: FETCH_ANSWERS_GET_BEGIN
  });
}

export const getAnswerSuccess = message => ({
  type: FETCH_ANSWERS_GET_SUCCESS,
  payload: { message }
});

export const getAnswerFailure = error => ({
  type: FETCH_ANSWERS_GET_FAILURE,
  payload: { error }
});

export function getAnswers(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getAnswerBegin());
    // If we aren't logged in, we can't get anything, so...
    if (!token) {
      dispatch(getAnswerFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + '/answers/get', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getAnswerFailure(json.error));
        } else {
          dispatch(getAnswerSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(getAnswerFailure(error)));
  };
}