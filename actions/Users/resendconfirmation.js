import config from '../../env';

export const FETCH_RESEND_CONFIRMATION_BEGIN   = 'FETCH_RESEND_CONFIRMATION_BEGIN';
export const FETCH_RESEND_CONFIRMATION_SUCCESS = 'FETCH_RESEND_CONFIRMATION_SUCCESS';
export const FETCH_RESEND_CONFIRMATION_FAILURE = 'FETCH_RESEND_CONFIRMATION_FAILURE';

export const resendConfirmationBegin = () => ({
  type: FETCH_RESEND_CONFIRMATION_BEGIN
});

export const resendConfirmationSuccess = message => ({
  type: FETCH_RESEND_CONFIRMATION_SUCCESS,
  payload: { message }
});

export const resendConfirmationFailure = error => ({
  type: FETCH_RESEND_CONFIRMATION_FAILURE,
  payload: { error }
});

export function resendConfirmation(email) {
  return dispatch => {
    dispatch(resendConfirmationBegin());
    return fetch(config.api + "/users/register/resend", {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({email})
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(resendConfirmationFailure(json.error));
        } else {
          dispatch(resendConfirmationSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(resendConfirmationFailure(error)));
  };
}