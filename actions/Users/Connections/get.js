import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_GET_CONNECTIONS_BEGIN   = 'FETCH_GET_CONNECTIONS_BEGIN';
export const FETCH_GET_CONNECTIONS_SUCCESS = 'FETCH_GET_CONNECTIONS_SUCCESS';
export const FETCH_GET_CONNECTIONS_FAILURE = 'FETCH_GET_CONNECTIONS_FAILURE';
export const FETCH_GET_CONNECTIONS_CLEAR_STATE = 'FETCH_GET_CONNECTIONS_CLEAR_STATE';

export const getConnectionsBegin = () => ({
  type: FETCH_GET_CONNECTIONS_BEGIN
});

export const getConnectionsSuccess = message => ({
  type: FETCH_GET_CONNECTIONS_SUCCESS,
  payload: { message }
});

export const getConnectionsFailure = error => ({
  type: FETCH_GET_CONNECTIONS_FAILURE,
  payload: { error }
});

export const getConnectionsClearState = result => ({
  type: FETCH_GET_CONNECTIONS_CLEAR_STATE,
  payload: {result}
});

export default function getConnections(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getConnectionsBegin());
    return fetch(config.api + "/connections", {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getConnectionsFailure(json.error));
        } else {
          dispatch(getConnectionsSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(getConnectionsFailure(error)));
  };
}