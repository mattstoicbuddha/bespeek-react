import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_CONNECTION_PROFILE_BEGIN   = 'FETCH_CONNECTION_PROFILE_BEGIN';
export const FETCH_CONNECTION_PROFILE_SUCCESS = 'FETCH_CONNECTION_PROFILE_SUCCESS';
export const FETCH_CONNECTION_PROFILE_FAILURE = 'FETCH_CONNECTION_PROFILE_FAILURE';
export const FETCH_CONNECTION_PROFILE_CLEAR_STATE = 'FETCH_CONNECTION_PROFILE_CLEAR_STATE';

export const getConnectionProfileBegin = () => ({
  type: FETCH_CONNECTION_PROFILE_BEGIN
});

export const getConnectionProfileSuccess = message => ({
  type: FETCH_CONNECTION_PROFILE_SUCCESS,
  payload: { message }
});

export const getConnectionProfileFailure = error => ({
  type: FETCH_CONNECTION_PROFILE_FAILURE,
  payload: { error }
});

export const getConnectionProfileClearState = result => ({
  type: FETCH_CONNECTION_PROFILE_CLEAR_STATE,
  payload: {result}
});

export function getConnectionProfile(connection_id) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getConnectionProfileBegin());
    return fetch(config.api + "/connections/profile/" + connection_id, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getConnectionProfileFailure(json.error));
        } else {
          dispatch(getConnectionProfileSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(getConnectionProfileFailure(error)));
  };
}