import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';
import analyticsHelper from '../../../helpers/analytics';

export const FETCH_DELETE_CONNECTIONS_BEGIN   = 'FETCH_DELETE_CONNECTIONS_BEGIN';
export const FETCH_DELETE_CONNECTIONS_SUCCESS = 'FETCH_DELETE_CONNECTIONS_SUCCESS';
export const FETCH_DELETE_CONNECTIONS_FAILURE = 'FETCH_DELETE_CONNECTIONS_FAILURE';
export const FETCH_DELETE_CONNECTIONS_CLEAR_STATE = 'FETCH_DELETE_CONNECTIONS_CLEAR_STATE';

export const deleteConnectionsBegin = () => ({
  type: FETCH_DELETE_CONNECTIONS_BEGIN
});

export const deleteConnectionsSuccess = message => ({
  type: FETCH_DELETE_CONNECTIONS_SUCCESS,
  payload: { message }
});

export const deleteConnectionsFailure = error => ({
  type: FETCH_DELETE_CONNECTIONS_FAILURE,
  payload: { error }
});

export const deleteConnectionsClearState = result => ({
  type: FETCH_DELETE_CONNECTIONS_CLEAR_STATE,
  payload: {result}
});

export default function deleteConnection(id) {
  let code = 200;
  const token = cookieHelper.getTokenCookie();
  const connection_ids = [id];
  return dispatch => {
    dispatch(deleteConnectionsBegin());
    return fetch(config.api + "/connections/delete", {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({connection_ids})
      })
      .then(res => {code = res.status; return res.json()})
      .then(json => {
         let ev = "connections-delete";
        if (!json.success || code > 399) {
          ev += "-failure";
          dispatch(deleteConnectionsFailure(json));
        } else {
          let returns = json.response.results;
          if (Array.isArray(returns) && returns.length) returns = returns.pop();
          dispatch(deleteConnectionsSuccess(returns));
        }
        analyticsHelper.send(ev, "dashboard");
        return json;
      })
      .catch(error => {
        dispatch(deleteConnectionsFailure(error))
      });
  };
}