import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';
import analyticsHelper from '../../../helpers/analytics';

export const FETCH_ADD_CONNECTIONS_BEGIN   = 'FETCH_ADD_CONNECTIONS_BEGIN';
export const FETCH_ADD_CONNECTIONS_SUCCESS = 'FETCH_ADD_CONNECTIONS_SUCCESS';
export const FETCH_ADD_CONNECTIONS_FAILURE = 'FETCH_ADD_CONNECTIONS_FAILURE';
export const FETCH_ADD_CONNECTIONS_CLEAR_STATE = 'FETCH_ADD_CONNECTIONS_CLEAR_STATE';

export const addConnectionsBegin = () => ({
  type: FETCH_ADD_CONNECTIONS_BEGIN
});

export const addConnectionsSuccess = message => ({
  type: FETCH_ADD_CONNECTIONS_SUCCESS,
  payload: { message }
});

export const addConnectionsFailure = error => ({
  type: FETCH_ADD_CONNECTIONS_FAILURE,
  payload: { error }
});

export const addConnectionsClearState = result => ({
  type: FETCH_ADD_CONNECTIONS_CLEAR_STATE,
  payload: {result}
});

export default function addConnection(opts) {
  let code = 200;
  const token = cookieHelper.getTokenCookie();
  const connection_email = opts.email;
  return dispatch => {
    dispatch(addConnectionsBegin());
    return fetch(config.api + "/connections/add", {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({connection_email})
      })
      .then(res => {code = res.status; return res.json()})
      .then(json => {
        let ev = "connections-add";
        if (!json.success || code > 399) {
          ev += "-failure";
          dispatch(addConnectionsFailure(json));
        } else {
          let returns = json.response.results;
          if (Array.isArray(returns) && returns.length) returns = returns.pop();
          dispatch(addConnectionsSuccess(returns));
        }
        analyticsHelper.send(ev, "dashboard");
        return json;
      })
      .catch(error => {
        dispatch(addConnectionsFailure(error))
      });
  };
}