import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_RESET_PASSWORD_CHECK_BEGIN   = 'FETCH_RESET_PASSWORD_CHECK_BEGIN';
export const FETCH_RESET_PASSWORD_CHECK_SUCCESS = 'FETCH_RESET_PASSWORD_CHECK_SUCCESS';
export const FETCH_RESET_PASSWORD_CHECK_FAILURE = 'FETCH_RESET_PASSWORD_CHECK_FAILURE';

export const getPasswordResetCheckBegin = () => {
  return ({
    type: FETCH_RESET_PASSWORD_CHECK_BEGIN
  });
}

export const getPasswordResetCheckSuccess = message => ({
  type: FETCH_RESET_PASSWORD_CHECK_SUCCESS,
  payload: { message }
});

export const getPasswordResetCheckFailure = error => ({
  type: FETCH_RESET_PASSWORD_CHECK_FAILURE,
  payload: { error }
});

export function getPasswordResetCheck(code) {
  return dispatch => {
    dispatch(getPasswordResetCheckBegin());
    return fetch(config.api + '/users/passwordreset/check?reset_code=' + code, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getPasswordResetCheckFailure(json.error));
        } else {
          dispatch(getPasswordResetCheckSuccess(json.response));
        }
        return json;
      })
      .catch(error => {
        console.log({error})
        dispatch(getPasswordResetCheckFailure(error))});
  };
}