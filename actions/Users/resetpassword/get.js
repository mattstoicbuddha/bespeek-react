import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_RESET_PASSWORD_BEGIN   = 'FETCH_RESET_PASSWORD_BEGIN';
export const FETCH_RESET_PASSWORD_SUCCESS = 'FETCH_RESET_PASSWORD_SUCCESS';
export const FETCH_RESET_PASSWORD_FAILURE = 'FETCH_RESET_PASSWORD_FAILURE';

export const getPasswordResetBegin = () => {
  return ({
    type: FETCH_RESET_PASSWORD_BEGIN
  });
}

export const getPasswordResetSuccess = message => ({
  type: FETCH_RESET_PASSWORD_SUCCESS,
  payload: { message }
});

export const getPasswordResetFailure = error => ({
  type: FETCH_RESET_PASSWORD_FAILURE,
  payload: { error }
});

export function getPasswordReset(email) {
  return dispatch => {
    dispatch(getPasswordResetBegin());
    return fetch(config.api + '/users/passwordreset?email=' + email, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getPasswordResetFailure(json.error));
        } else {
          dispatch(getPasswordResetSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(getPasswordResetFailure(error)));
  };
}