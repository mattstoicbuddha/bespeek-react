import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_RESET_PASSWORD_SAVE_BEGIN   = 'FETCH_RESET_PASSWORD_SAVE_BEGIN';
export const FETCH_RESET_PASSWORD_SAVE_SUCCESS = 'FETCH_RESET_PASSWORD_SAVE_SUCCESS';
export const FETCH_RESET_PASSWORD_SAVE_FAILURE = 'FETCH_RESET_PASSWORD_SAVE_FAILURE';

export const savePasswordResetBegin = () => {
  return ({
    type: FETCH_RESET_PASSWORD_SAVE_BEGIN
  });
}

export const savePasswordResetSuccess = message => ({
  type: FETCH_RESET_PASSWORD_SAVE_SUCCESS,
  payload: { message }
});

export const savePasswordResetFailure = error => ({
  type: FETCH_RESET_PASSWORD_SAVE_FAILURE,
  payload: { error }
});

export function savePasswordReset(opts) {
  const body = {
    reset_code: opts.code,
    password_first: opts.password,
    password_second: opts.password2
  }
  return dispatch => {
    dispatch(savePasswordResetBegin());
    return fetch(config.api + '/users/passwordreset', {
        method: 'put',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(json => {
        console.log({json})
        if (!json.success) {
          dispatch(savePasswordResetFailure(json.error));
        } else {
          dispatch(savePasswordResetSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(savePasswordResetFailure(error)));
  };
}