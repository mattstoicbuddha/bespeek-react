import config from '../../env';

export const FETCH_CONFIRM_USER_REGISTRATION_BEGIN   = 'FETCH_CONFIRM_USER_REGISTRATION_BEGIN';
export const FETCH_CONFIRM_USER_REGISTRATION_SUCCESS = 'FETCH_CONFIRM_USER_REGISTRATION_SUCCESS';
export const FETCH_CONFIRM_USER_REGISTRATION_FAILURE = 'FETCH_CONFIRM_USER_REGISTRATION_FAILURE';

export const confirmUserRegistrationBegin = () => ({
  type: FETCH_CONFIRM_USER_REGISTRATION_BEGIN
});

export const confirmUserRegistrationSuccess = message => ({
  type: FETCH_CONFIRM_USER_REGISTRATION_SUCCESS,
  payload: { message }
});

export const confirmUserRegistrationFailure = error => ({
  type: FETCH_CONFIRM_USER_REGISTRATION_FAILURE,
  payload: { error }
});

export function confirmUserRegistration(code) {
  return dispatch => {
    dispatch(confirmUserRegistrationBegin());
    return fetch(config.api + "/users/register/confirm", {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({confirmation_code: code})
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(confirmUserRegistrationFailure(json.error));
        } else {
          dispatch(confirmUserRegistrationSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(confirmUserRegistrationFailure(error)));
  };
}