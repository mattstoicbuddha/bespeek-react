import config from '../../env';

export const FETCH_USER_REGISTRATION_BEGIN   = 'FETCH_USER_REGISTRATION_BEGIN';
export const FETCH_USER_REGISTRATION_SUCCESS = 'FETCH_USER_REGISTRATION_SUCCESS';
export const FETCH_USER_REGISTRATION_FAILURE = 'FETCH_USER_REGISTRATION_FAILURE';

export const registerUserBegin = () => ({
  type: FETCH_USER_REGISTRATION_BEGIN
});

export const registerUserSuccess = message => ({
  type: FETCH_USER_REGISTRATION_SUCCESS,
  payload: { message }
});

export const registerUserFailure = error => ({
  type: FETCH_USER_REGISTRATION_FAILURE,
  payload: { error }
});

export function registerUser(opts) {
  return dispatch => {
    dispatch(registerUserBegin());
    return fetch(config.api + "/users/register", {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(opts)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(registerUserFailure(json.error));
        } else {
          dispatch(registerUserSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(registerUserFailure(error)));
  };
}