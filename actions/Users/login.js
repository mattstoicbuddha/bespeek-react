import config from '../../env';
import analyticsHelper from '../../helpers/analytics';

export const FETCH_USER_LOGIN_BEGIN   = 'FETCH_USER_LOGIN_BEGIN';
export const FETCH_USER_LOGIN_SUCCESS = 'FETCH_USER_LOGIN_SUCCESS';
export const FETCH_USER_LOGIN_FAILURE = 'FETCH_USER_LOGIN_FAILURE';
export const FETCH_USER_LOGIN_CLEAR_STATE = 'FETCH_USER_LOGIN_CLEAR_STATE';

export const loginUserBegin = () => ({
  type: FETCH_USER_LOGIN_BEGIN
});

export const loginUserSuccess = message => ({
  type: FETCH_USER_LOGIN_SUCCESS,
  payload: { message }
});

export const loginUserFailure = error => ({
  type: FETCH_USER_LOGIN_FAILURE,
  payload: { error }
});

export const loginUserClearState = result => ({
  type: FETCH_USER_LOGIN_CLEAR_STATE,
  payload: {result}
});

export function loginUser(opts) {
  let code = 200;
  return dispatch => {
    dispatch(loginUserBegin());
    return fetch(config.api + "/users/authenticate", {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(opts)
      })
      .then(res => {code = res.status; return res.json()})
      .then(json => {
        let ev = "login";
        if (!json.success || code > 399) {
          ev += "-failure";
          dispatch(loginUserFailure(json.error));
        } else {
          dispatch(loginUserSuccess(json));
        }
        analyticsHelper.send(ev, "website");
        return json;
      })
      .catch(error => {
        analyticsHelper.send("login-failure", "website");
        return dispatch(loginUserFailure(error));
      });
  };
}