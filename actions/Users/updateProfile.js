import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_USER_PROFILE_UPDATE_BEGIN   = 'FETCH_USER_PROFILE_UPDATE_BEGIN';
export const FETCH_USER_PROFILE_UPDATE_SUCCESS = 'FETCH_USER_PROFILE_UPDATE_SUCCESS';
export const FETCH_USER_PROFILE_UPDATE_FAILURE = 'FETCH_USER_PROFILE_UPDATE_FAILURE';
export const FETCH_USER_PROFILE_UPDATE_CLEAR_STATE = 'FETCH_USER_PROFILE_UPDATE_CLEAR_STATE';

export const updateUserProfileBegin = () => ({
  type: FETCH_USER_PROFILE_UPDATE_BEGIN
});

export const updateUserProfileSuccess = message => ({
  type: FETCH_USER_PROFILE_UPDATE_SUCCESS,
  payload: { message }
});

export const updateUserProfileFailure = error => ({
  type: FETCH_USER_PROFILE_UPDATE_FAILURE,
  payload: { error }
});

export const updateUserProfileClearState = result => ({
  type: FETCH_USER_PROFILE_UPDATE_CLEAR_STATE,
  payload: {result}
});

export function updateUserProfile(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    console.log("Here?");
    dispatch(updateUserProfileBegin());
    console.log("Here2?");
    return fetch(config.api + "/users/profile", {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(opts)
      })
      .then(res => res.json())
      .then(json => {
        console.log({json})
        if (!json.success) {
          dispatch(updateUserProfileFailure(json.error));
        } else {
          dispatch(updateUserProfileSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(updateUserProfileFailure(error)));
  };
}