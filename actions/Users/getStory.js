import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_USER_STORY_BEGIN   = 'FETCH_USER_STORY_BEGIN';
export const FETCH_USER_STORY_SUCCESS = 'FETCH_USER_STORY_SUCCESS';
export const FETCH_USER_STORY_FAILURE = 'FETCH_USER_STORY_FAILURE';
export const FETCH_USER_STORY_CLEAR_STATE = 'FETCH_USER_STORY_CLEAR_STATE';

export const getUserStoryBegin = () => ({
  type: FETCH_USER_STORY_BEGIN
});

export const getUserStorySuccess = message => ({
  type: FETCH_USER_STORY_SUCCESS,
  payload: { message }
});

export const getUserStoryFailure = error => ({
  type: FETCH_USER_STORY_FAILURE,
  payload: { error }
});

export const getUserStoryClearState = result => ({
  type: FETCH_USER_STORY_CLEAR_STATE,
  payload: {result}
});

export function getUserStory(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getUserStoryBegin());
    return fetch(config.api + "/users/story", {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(opts)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getUserStoryFailure(json.error));
        } else {
          dispatch(getUserStorySuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(getUserStoryFailure(error)));
  };
}