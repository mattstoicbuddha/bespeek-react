import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_USER_PROFILE_BEGIN   = 'FETCH_USER_PROFILE_BEGIN';
export const FETCH_USER_PROFILE_SUCCESS = 'FETCH_USER_PROFILE_SUCCESS';
export const FETCH_USER_PROFILE_FAILURE = 'FETCH_USER_PROFILE_FAILURE';
export const FETCH_USER_PROFILE_CLEAR_STATE = 'FETCH_USER_PROFILE_CLEAR_STATE';

export const getUserProfileBegin = () => ({
  type: FETCH_USER_PROFILE_BEGIN
});

export const getUserProfileSuccess = message => ({
  type: FETCH_USER_PROFILE_SUCCESS,
  payload: { message }
});

export const getUserProfileFailure = error => ({
  type: FETCH_USER_PROFILE_FAILURE,
  payload: { error }
});

export const getUserProfileClearState = result => ({
  type: FETCH_USER_PROFILE_CLEAR_STATE,
  payload: {result}
});

export function getUserProfile(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getUserProfileBegin());
    return fetch(config.api + "/users/profile", {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getUserProfileFailure(json.error));
        } else {
          dispatch(getUserProfileSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(getUserProfileFailure(error)));
  };
}