import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_QUESTION_BYID_GET_BEGIN   = 'FETCH_QUESTION_BYID_GET_BEGIN';
export const FETCH_QUESTION_BYID_GET_SUCCESS = 'FETCH_QUESTION_BYID_GET_SUCCESS';
export const FETCH_QUESTION_BYID_GET_FAILURE = 'FETCH_QUESTION_BYID_GET_FAILURE';

export const getQuestionByIdBegin = () => {
  return ({
    type: FETCH_QUESTION_BYID_GET_BEGIN
  });
}

export const getQuestionByIdSuccess = message => ({
  type: FETCH_QUESTION_BYID_GET_SUCCESS,
  payload: { message }
});

export const getQuestionByIdFailure = error => ({
  type: FETCH_QUESTION_BYID_GET_FAILURE,
  payload: { error }
});

export function getQuestionById(qid) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getQuestionByIdBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(getQuestionByIdFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + '/questions/byid/' + qid, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getQuestionByIdFailure(json.error));
        } else {
          dispatch(getQuestionByIdSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(getQuestionByIdFailure(error)));
  };
}