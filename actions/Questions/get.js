import config from '../../env';
import cookieHelper from '../../helpers/cookies';
import analyticsHelper from '../../helpers/analytics';

export const FETCH_QUESTION_GET_BEGIN   = 'FETCH_QUESTION_GET_BEGIN';
export const FETCH_QUESTION_GET_SUCCESS = 'FETCH_QUESTION_GET_SUCCESS';
export const FETCH_QUESTION_GET_FAILURE = 'FETCH_QUESTION_GET_FAILURE';

export const getQuestionBegin = () => {
  return ({
    type: FETCH_QUESTION_GET_BEGIN
  });
}

export const getQuestionSuccess = message => ({
  type: FETCH_QUESTION_GET_SUCCESS,
  payload: { message }
});

export const getQuestionFailure = error => ({
  type: FETCH_QUESTION_GET_FAILURE,
  payload: { error }
});

export function getQuestion(opts) {
  let code = 200;
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getQuestionBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(getQuestionFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + '/questions/new', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => {code = res.status; return res.json()})
      .then(json => {
        let ev = "question-get";
        if (!json.success || code > 399) {
          ev = "question-get-failure";
          dispatch(getQuestionFailure(json));
        } else {
          dispatch(getQuestionSuccess(json.response));
        }
        analyticsHelper.send(ev, "dashboard");
        return json;
      })
      .catch(error => {
        analyticsHelper.send("question-get-failure", "dashboard");
        return dispatch(getQuestionFailure(error))
      });
  };
}