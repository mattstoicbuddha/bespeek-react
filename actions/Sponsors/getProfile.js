import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_SPONSOR_PROFILE_BEGIN   = 'FETCH_SPONSOR_PROFILE_BEGIN';
export const FETCH_SPONSOR_PROFILE_SUCCESS = 'FETCH_SPONSOR_PROFILE_SUCCESS';
export const FETCH_SPONSOR_PROFILE_FAILURE = 'FETCH_SPONSOR_PROFILE_FAILURE';
export const FETCH_SPONSOR_PROFILE_CLEAR_STATE = 'FETCH_SPONSOR_PROFILE_CLEAR_STATE';

export const getSponsorProfileBegin = () => ({
  type: FETCH_SPONSOR_PROFILE_BEGIN
});

export const getSponsorProfileSuccess = message => ({
  type: FETCH_SPONSOR_PROFILE_SUCCESS,
  payload: { message }
});

export const getSponsorProfileFailure = error => ({
  type: FETCH_SPONSOR_PROFILE_FAILURE,
  payload: { error }
});

export const getSponsorProfileClearState = result => ({
  type: FETCH_SPONSOR_PROFILE_CLEAR_STATE,
  payload: {result}
});

export function getSponsorProfile(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getSponsorProfileBegin());
    return fetch(config.api + "/sponsors/profile", {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(opts)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getSponsorProfileFailure(json.error));
        } else {
          dispatch(getSponsorProfileSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(getSponsorProfileFailure(error)));
  };
}