import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_UPDATE_SPONSOR_PROFILE_BEGIN   = 'FETCH_UPDATE_SPONSOR_PROFILE_BEGIN';
export const FETCH_UPDATE_SPONSOR_PROFILE_SUCCESS = 'FETCH_UPDATE_SPONSOR_PROFILE_SUCCESS';
export const FETCH_UPDATE_SPONSOR_PROFILE_FAILURE = 'FETCH_UPDATE_SPONSOR_PROFILE_FAILURE';
export const FETCH_UPDATE_SPONSOR_PROFILE_CLEAR_STATE = 'FETCH_UPDATE_SPONSOR_PROFILE_CLEAR_STATE';

export const updateSponsorProfileBegin = () => ({
  type: FETCH_UPDATE_SPONSOR_PROFILE_BEGIN
});

export const updateSponsorProfileSuccess = message => ({
  type: FETCH_UPDATE_SPONSOR_PROFILE_SUCCESS,
  payload: { message }
});

export const updateSponsorProfileFailure = error => ({
  type: FETCH_UPDATE_SPONSOR_PROFILE_FAILURE,
  payload: { error }
});

export const updateSponsorProfileClearState = result => ({
  type: FETCH_UPDATE_SPONSOR_PROFILE_CLEAR_STATE,
  payload: {result}
});

export function updateSponsorProfile(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(updateSponsorProfileBegin());
    return fetch(config.api + "/sponsors/update", {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(opts)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(updateSponsorProfileFailure(json.error));
        } else {
          dispatch(updateSponsorProfileSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(updateSponsorProfileFailure(error)));
  };
}