import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_UPDATE_EMBED_CODE_SETTINGS_BEGIN   = 'FETCH_UPDATE_EMBED_CODE_SETTINGS_BEGIN';
export const FETCH_UPDATE_EMBED_CODE_SETTINGS_SUCCESS = 'FETCH_UPDATE_EMBED_CODE_SETTINGS_SUCCESS';
export const FETCH_UPDATE_EMBED_CODE_SETTINGS_FAILURE = 'FETCH_UPDATE_EMBED_CODE_SETTINGS_FAILURE';
export const FETCH_UPDATE_EMBED_CODE_SETTINGS_CLEAR_STATE = 'FETCH_UPDATE_EMBED_CODE_SETTINGS_CLEAR_STATE';

export const updateEmbedCodeSettingsBegin = () => ({
  type: FETCH_UPDATE_EMBED_CODE_SETTINGS_BEGIN
});

export const updateEmbedCodeSettingsSuccess = message => ({
  type: FETCH_UPDATE_EMBED_CODE_SETTINGS_SUCCESS,
  payload: { message }
});

export const updateEmbedCodeSettingsFailure = error => ({
  type: FETCH_UPDATE_EMBED_CODE_SETTINGS_FAILURE,
  payload: { error }
});

export const updateEmbedCodeSettingsClearState = result => ({
  type: FETCH_UPDATE_EMBED_CODE_SETTINGS_CLEAR_STATE,
  payload: {result}
});

export function updateEmbedCodeSettings(opts) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(updateEmbedCodeSettingsBegin());
    return fetch(config.api + "/sponsors/embedcode/settings", {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(opts)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(updateEmbedCodeSettingsFailure(json.error));
        } else {
          dispatch(updateEmbedCodeSettingsSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(updateEmbedCodeSettingsFailure(error)));
  };
}