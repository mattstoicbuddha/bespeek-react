import {getSponsorProfile as getProfile} from './getProfile';
import {updateSponsorProfile as updateProfile} from './updateProfile';
import {getEmbedCodeSettings} from './getEmbedCodeSettings';
import {updateEmbedCodeSettings} from './updateEmbedCodeSettings';

export {
	getProfile,
	updateProfile,
	getEmbedCodeSettings,
	updateEmbedCodeSettings
}