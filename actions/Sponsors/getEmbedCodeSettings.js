import config from '../../env';
import cookieHelper from '../../helpers/cookies';

export const FETCH_GET_EMBED_CODE_SETTINGS_BEGIN   = 'FETCH_GET_EMBED_CODE_SETTINGS_BEGIN';
export const FETCH_GET_EMBED_CODE_SETTINGS_SUCCESS = 'FETCH_GET_EMBED_CODE_SETTINGS_SUCCESS';
export const FETCH_GET_EMBED_CODE_SETTINGS_FAILURE = 'FETCH_GET_EMBED_CODE_SETTINGS_FAILURE';
export const FETCH_GET_EMBED_CODE_SETTINGS_CLEAR_STATE = 'FETCH_GET_EMBED_CODE_SETTINGS_CLEAR_STATE';

export const getEmbedCodeSettingsBegin = () => ({
  type: FETCH_GET_EMBED_CODE_SETTINGS_BEGIN
});

export const getEmbedCodeSettingsSuccess = message => ({
  type: FETCH_GET_EMBED_CODE_SETTINGS_SUCCESS,
  payload: { message }
});

export const getEmbedCodeSettingsFailure = error => ({
  type: FETCH_GET_EMBED_CODE_SETTINGS_FAILURE,
  payload: { error }
});

export const getEmbedCodeSettingsClearState = result => ({
  type: FETCH_GET_EMBED_CODE_SETTINGS_CLEAR_STATE,
  payload: {result}
});

export function getEmbedCodeSettings() {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getEmbedCodeSettingsBegin());
    return fetch(config.api + "/sponsors/embedcode/settings", {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        console.log({json})
        if (!json.success) {
          dispatch(getEmbedCodeSettingsFailure(json.error));
        } else {
          dispatch(getEmbedCodeSettingsSuccess(json));
        }
        return json;
      })
      .catch(error => dispatch(getEmbedCodeSettingsFailure(error)));
  };
}