import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_QUESTION_LIST_GET_BEGIN   = 'FETCH_QUESTION_LIST_GET_BEGIN';
export const FETCH_QUESTION_LIST_GET_SUCCESS = 'FETCH_QUESTION_LIST_GET_SUCCESS';
export const FETCH_QUESTION_LIST_GET_FAILURE = 'FETCH_QUESTION_LIST_GET_FAILURE';

export const getQuestionListBegin = () => {
  return ({
    type: FETCH_QUESTION_LIST_GET_BEGIN
  });
}

export const getQuestionListSuccess = message => ({
  type: FETCH_QUESTION_LIST_GET_SUCCESS,
  payload: { message }
});

export const getQuestionListFailure = error => ({
  type: FETCH_QUESTION_LIST_GET_FAILURE,
  payload: { error }
});

export function getQuestionList() {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getQuestionListBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(getQuestionListFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/questions/list', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getQuestionListFailure(json.error));
        } else {
          dispatch(getQuestionListSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(getQuestionListFailure(error)));
  };
}