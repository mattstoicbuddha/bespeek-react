import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_QUESTION_SAVE_BEGIN   = 'FETCH_QUESTION_SAVE_BEGIN';
export const FETCH_QUESTION_SAVE_SUCCESS = 'FETCH_QUESTION_SAVE_SUCCESS';
export const FETCH_QUESTION_SAVE_FAILURE = 'FETCH_QUESTION_SAVE_FAILURE';

export const saveQuestionBegin = () => {
  return ({
    type: FETCH_QUESTION_SAVE_BEGIN
  });
}

export const saveQuestionSuccess = message => ({
  type: FETCH_QUESTION_SAVE_SUCCESS,
  payload: { message }
});

export const saveQuestionFailure = error => ({
  type: FETCH_QUESTION_SAVE_FAILURE,
  payload: { error }
});

export function saveQuestion(opts) {
  console.log({opts})
  const token = cookieHelper.getTokenCookie();
  let action = "create",
      method = "post";
  const body = {
    text: opts.text,
    category_id: opts.categoryId,
    deleted: opts.deleted
  }
  // Make sure we are using the put method to
  // the update route if we have a question
  // id; otherwise, the API will throw an error
  if (opts.questionId) {
    action = "update";
    method = "put";
    // Make sure we are sending our question id
    // so the API knows which question we are saving
    body.id = opts.questionId;
  } else {
    // If we are creating a new question, we need
    // to pass the type; note that we can't do
    // this if we are updating a question
    body.type = opts.type;
  }
  if (opts.type === "boolean") {
    body.true_cat_id = opts.true_cat_id;
    body.false_cat_id = opts.false_cat_id;
  }
  if (opts.type === "multiple_choice") {
    body.possible_answers = opts.possible_answers;
  }

  return dispatch => {
    dispatch(saveQuestionBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(saveQuestionFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/questions/' + action, {
        method: method,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(saveQuestionFailure(json.error));
        } else {
          dispatch(saveQuestionSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(saveQuestionFailure(error)));
  };
}