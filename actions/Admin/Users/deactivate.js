import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_DEACTIVATE_USER_BEGIN   = 'FETCH_DEACTIVATE_USER_BEGIN';
export const FETCH_DEACTIVATE_USER_SUCCESS = 'FETCH_DEACTIVATE_USER_SUCCESS';
export const FETCH_DEACTIVATE_USER_FAILURE = 'FETCH_DEACTIVATE_USER_FAILURE';

export const deactivateUserBegin = () => {
  return ({
    type: FETCH_DEACTIVATE_USER_BEGIN
  });
}

export const deactivateUserSuccess = message => ({
  type: FETCH_DEACTIVATE_USER_SUCCESS,
  payload: { message }
});

export const deactivateUserFailure = error => ({
  type: FETCH_DEACTIVATE_USER_FAILURE,
  payload: { error }
});

export function deactivateUser(id) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(deactivateUserBegin());// If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(deactivateUserFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/users/deactivate', {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({user_id: id})
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(deactivateUserFailure(json.error));
        } else {
          dispatch(deactivateUserSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(deactivateUserFailure(error)));
  };
}