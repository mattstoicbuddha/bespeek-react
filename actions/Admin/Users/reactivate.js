import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_REACTIVATE_USER_BEGIN   = 'FETCH_REACTIVATE_USER_BEGIN';
export const FETCH_REACTIVATE_USER_SUCCESS = 'FETCH_REACTIVATE_USER_SUCCESS';
export const FETCH_REACTIVATE_USER_FAILURE = 'FETCH_REACTIVATE_USER_FAILURE';

export const reactivateUserBegin = () => {
  return ({
    type: FETCH_REACTIVATE_USER_BEGIN
  });
}

export const reactivateUserSuccess = message => ({
  type: FETCH_REACTIVATE_USER_SUCCESS,
  payload: { message }
});

export const reactivateUserFailure = error => ({
  type: FETCH_REACTIVATE_USER_FAILURE,
  payload: { error }
});

export function reactivateUser(id) {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(reactivateUserBegin());// If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(reactivateUserFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/users/reactivate', {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({user_id: id})
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(reactivateUserFailure(json.error));
        } else {
          dispatch(reactivateUserSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(reactivateUserFailure(error)));
  };
}