import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_USERS_LIST_GET_BEGIN   = 'FETCH_USERS_LIST_GET_BEGIN';
export const FETCH_USERS_LIST_GET_SUCCESS = 'FETCH_USERS_LIST_GET_SUCCESS';
export const FETCH_USERS_LIST_GET_FAILURE = 'FETCH_USERS_LIST_GET_FAILURE';

export const getUsersListBegin = () => {
  return ({
    type: FETCH_USERS_LIST_GET_BEGIN
  });
}

export const getUsersListSuccess = message => ({
  type: FETCH_USERS_LIST_GET_SUCCESS,
  payload: { message }
});

export const getUsersListFailure = error => ({
  type: FETCH_USERS_LIST_GET_FAILURE,
  payload: { error }
});

export function getUsersList() {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getUsersListBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(getUsersListFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/users/list', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getUsersListFailure(json.error));
        } else {
          dispatch(getUsersListSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(getUsersListFailure(error)));
  };
}