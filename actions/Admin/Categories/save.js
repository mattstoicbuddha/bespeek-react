import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_QUESTION_CATEGORIES_SAVE_BEGIN   = 'FETCH_QUESTION_CATEGORIES_SAVE_BEGIN';
export const FETCH_QUESTION_CATEGORIES_SAVE_SUCCESS = 'FETCH_QUESTION_CATEGORIES_SAVE_SUCCESS';
export const FETCH_QUESTION_CATEGORIES_SAVE_FAILURE = 'FETCH_QUESTION_CATEGORIES_SAVE_FAILURE';

export const saveCategoryBegin = () => {
  return ({
    type: FETCH_QUESTION_CATEGORIES_SAVE_BEGIN
  });
}

export const saveCategorySuccess = message => ({
  type: FETCH_QUESTION_CATEGORIES_SAVE_SUCCESS,
  payload: { message }
});

export const saveCategoryFailure = error => ({
  type: FETCH_QUESTION_CATEGORIES_SAVE_FAILURE,
  payload: { error }
});

export function saveCategory(opts) {
  console.log({opts})
  const token = cookieHelper.getTokenCookie();
  let action = "create",
      method = "post";
  const body = {
    category_name: opts.category_name,
    category_description: opts.category_description
  }
  // Make sure we are using the put method to
  // the update route if we have a question
  // id; otherwise, the API will throw an error
  if (opts.categoryId) {
    action = "update";
    method = "put";
    // Make sure we are sending our question id
    // so the API knows which question we are saving
    body.id = opts.categoryId;
  }

  return dispatch => {
    dispatch(saveCategoryBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(saveCategoryFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/categories/' + action, {
        method: method,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(saveCategoryFailure(json.error));
        } else {
          dispatch(saveCategorySuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(saveCategoryFailure(error)));
  };
}