import config from '../../../env';
import cookieHelper from '../../../helpers/cookies';

export const FETCH_QUESTION_CATEGORIES_LIST_GET_BEGIN   = 'FETCH_QUESTION_CATEGORIES_LIST_GET_BEGIN';
export const FETCH_QUESTION_CATEGORIES_LIST_GET_SUCCESS = 'FETCH_QUESTION_CATEGORIES_LIST_GET_SUCCESS';
export const FETCH_QUESTION_CATEGORIES_LIST_GET_FAILURE = 'FETCH_QUESTION_CATEGORIES_LIST_GET_FAILURE';

export const getQuestionCategoriesListBegin = () => {
  return ({
    type: FETCH_QUESTION_CATEGORIES_LIST_GET_BEGIN
  });
}

export const getQuestionCategoriesListSuccess = message => ({
  type: FETCH_QUESTION_CATEGORIES_LIST_GET_SUCCESS,
  payload: { message }
});

export const getQuestionCategoriesListFailure = error => ({
  type: FETCH_QUESTION_CATEGORIES_LIST_GET_FAILURE,
  payload: { error }
});

export function getQuestionCategoriesList() {
  const token = cookieHelper.getTokenCookie();
  return dispatch => {
    dispatch(getQuestionCategoriesListBegin());
    // If we aren't logged in, we can't save anything, so...
    if (!token) {
      dispatch(getQuestionCategoriesListFailure("You are not logged in."));
      return {};
    }
    return fetch(config.api + config.adminApiPrefix + '/categories/list', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          dispatch(getQuestionCategoriesListFailure(json.error));
        } else {
          dispatch(getQuestionCategoriesListSuccess(json.response));
        }
        return json;
      })
      .catch(error => dispatch(getQuestionCategoriesListFailure(error)));
  };
}