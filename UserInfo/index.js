import React, { Component } from 'react';
import Router from 'next/router';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './style.scss';

class UserInfo extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {userInfo} = this.props;
    // If we don't have user info yet, there is nothing to show
    if (!userInfo) return (<div></div>);
    // If we *do* have user info, we need to display it
    return (
      <div>
        <div className="user-info first-name">{userInfo.first_name}</div>
        <div className="user-info last-name">{userInfo.last_name}</div>
        <div className="user-info email">{userInfo.email}</div>
      </div>
    );
  }
}

export default UserInfo;