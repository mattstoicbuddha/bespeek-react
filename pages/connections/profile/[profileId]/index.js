import Link from 'next/link'
import Layout from '../../../../components/Layout';
import ConnectionProfile from '../../../../components/Connections/Profile'

export default (pageProps) => {
  const { profileId } = pageProps.query;
	return (
		<Layout title={"Connection Profile"}>
			<div className={"connection-profile "}>
				<ConnectionProfile query={{profileId}} />
			</div>
	  	</Layout>
	)
}