import Link from 'next/link'
import Layout from '../../components/Layout';
const env = process.env.REACT_APP_NODE_ENV;
import './style.scss';

export default () => (
	<Layout title={"Bespeek.com"}>
	
		<div className="home-page">
			<div className="home-hero">
				<div className="home-cta">
				<p>Bespeek allows you to plan a little at a time. Continuously answering questions until your final wishes plan is complete. Your membership to Bespeek gives you access to your planning profile for life!</p>
					<a className="btn btn-home-hero" href="/about"><img className="btn-home-hero-logo" src="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/bee-white.png"/> Learn More</a>
				</div>
				<img className="home-hero-bg" src="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/red-clock-bg2.jpg" />
			</div>
			
		</div>
	
  	</Layout>
)

