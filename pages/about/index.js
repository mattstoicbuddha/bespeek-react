import Link from 'next/link'
import Layout from '../../components/Layout';
import '../../components/Layout/global.scss';
import './style.scss';

export default () => (
	<Layout title={"About Bespeek"}>
	<div className="about-hero">
		<img src="https://bespeek-react-assets.s3-us-west-2.amazonaws.com/o-father-son-talking.jpg" />
	</div>
		<div className="about-us">
			<p>On social media we must sensor ourselves. We create associations with friends and family, but then we hide most of the real "Us" from others. Our legacy is left to consist of primarily pet videos and pictures of appetizer plates.
			So it goes on social media. We hide personal information, and for a good reason! We don’t want to affect others or our selves by making certain information or opinions public.
			So what if we could control who saw certain information, and better yet, what if we could totally control when that information was released?
			Why then you would have Bespeek!</p>
			
			<div className="vid-container">
				<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AMlJJAaDZS0" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div className="vid-paragraph-container">
			<p>Bespeek allows you to show the real you, but only to those you choose and only at the time you select. When you register and pay a one-time fee, you will have access to Bespeek for life.
			You can organize your content into separate Journals on Bespeek. You can change, re-arrange and change your content again. Add more Journals as the years pass. Choose to create a time-line or a media library.
			Bespeek holds the content and tells the story of your life using life Journals.</p>
			</div>
		</div>
		
  	</Layout>
)