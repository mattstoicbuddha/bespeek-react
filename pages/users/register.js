import Link from 'next/link';
import Layout from '../../components/Layout';
import RegistrationForm from '../../components/Users/RegistrationForm';

export default () => (
	<Layout title={"Register"}>
		<div className={"register col-sm-6 offset-sm-3 bg-white"}>
			<RegistrationForm />
		</div>
  	</Layout>
)