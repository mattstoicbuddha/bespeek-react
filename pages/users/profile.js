import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import Layout from '../../components/Layout';
import cookieHelper from '../../helpers/cookies';
import {UserInfo, AnsweredQuestions, Connections, SponsorInfo} from '../../components/Users/Profile';
import './style.scss';

class showProfile extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount(){
		let token = cookieHelper.getTokenCookie();
		if (!token) {
		  Router.push("/users/login");
		}
	}

	render() {
		return (
			<Layout title={"Profile"}>
			<div className="flexrow">
				<div className="profile-data bg-white profile-item">
					<UserInfo editable={true} />
				</div>
				<div className="profile-connections bg-white profile-item">
					<Connections />
				</div>

			</div>
			<div className="sponsor-banner">
				<SponsorInfo />
			</div>
				<div className="profile bg-white">
					<AnsweredQuestions />
				</div>
  		</Layout>
	  	)
 	}
}

const mapStateToProps = (state) => {
    return {
      result: state.UserProfile.result,
      loading: state.UserProfile.loading,
      error: state.UserProfile.error
    };
}

export default showProfile;