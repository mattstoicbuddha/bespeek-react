import Link from 'next/link';
import Layout from '../../components/Layout';
import ConfirmRegistrationCheck from '../../components/Users/ConfirmRegistrationCheck'

export default (pageProps) => {
	return (
		<Layout title={"Confirm Registration"}>
			<div className={"confirm-registration col-sm-6 offset-sm-3"}>
				<ConfirmRegistrationCheck query={pageProps.query} />
			</div>
	  	</Layout>
	)
}