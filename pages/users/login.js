import Link from 'next/link';
import Layout from '../../components/Layout';
import LoginForm from '../../components/Users/LoginForm';

export default () => (
	<Layout title={"Login"}>
		<div className={"login col-sm-6 offset-sm-3"}>
			<LoginForm />
		</div>
  	</Layout>
)