import Link from 'next/link';
import Layout from '../../../../components/Layout';
import CreatePassword from '../../../../components/Users/CreatePassword'

export default (pageProps) => {
  const { createCode } = pageProps.query;
	return (
		<Layout title={"Create Password"}>
			<div className={"create-password col-sm-6 offset-sm-3"}>
				<CreatePassword query={{createCode}} />
			</div>
	  	</Layout>
	)
}