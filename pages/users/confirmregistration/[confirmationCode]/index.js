import Link from 'next/link';
import Layout from '../../../../components/Layout';
import ConfirmRegistrationCheck from '../../../../components/Users/ConfirmRegistrationCheck'

export default (pageProps) => {
  const { confirmationCode } = pageProps.query;
	return (
		<Layout title={"Confirm Registration"}>
			<div className={"confirm-registration col-sm-6 offset-sm-3"}>
				<ConfirmRegistrationCheck query={{confirmationCode}} />
			</div>
	  	</Layout>
	)
}