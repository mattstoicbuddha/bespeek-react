import Link from 'next/link';
import Layout from '../../components/Layout';
import ResetPasswordForm from '../../components/Users/ResetPasswordForm';

export default (pageProps) => (
	<Layout title={"Reset Password"}>
		<div className={"reset-password-form col-sm-6 offset-sm-3"}>
			<ResetPasswordForm query={pageProps.query} />
		</div>
  	</Layout>
)