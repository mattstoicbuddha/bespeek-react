import React, { Component } from 'react';
import Link from 'next/link';
import Layout from '../../components/Layout';
import Dashboard from '../../components/Sponsor/Dashboard';
import './style.scss';
const {SponsorInfo, EmbedCode, Sponsees} = Dashboard;

class showDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaded: false
		}
	}

	render() {
		return (
				<Layout title={"Dashboard"}>
					<div className="dashboard">
						<SponsorInfo />
						<EmbedCode />
						<Sponsees />
					</div>
	  		</Layout>
	  	)
 	}
}

export default showDashboard;