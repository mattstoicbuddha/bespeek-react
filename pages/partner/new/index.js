import React, { Component } from 'react';
import {connect} from "react-redux";
import Link from 'next/link'
import Layout from '../../../components/Layout';
import {Registration, Onboarding} from '../../../components/Sponsor';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import config from '../../../env';
import './style.scss';

class PartnerNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

	componentDidMount(){
		const script = document.createElement('script');
		script.type = "text/javascript";
		script.dataset.cfasync = 'false';
		script.innerHTML = "window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '2c855558-00f4-49c4-8e98-6581185b67b9', f: true }); done = true; } }; })();";
		document.querySelector('head').appendChild(script);
	}
    render(){

		return (
			<Layout title={"Partner Bespeek"} pageClass="partner-new">

				<Onboarding />

				<Registration />
		  	</Layout>
		)
	}
}

const mapStateToProps = (state) => {
    return {};
}

export default connect(mapStateToProps)(PartnerNew);