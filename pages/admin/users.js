import React, { Component } from 'react';
import {connect} from "react-redux";
import config from "../../env";
import Link from 'next/link';
import Layout from '../../components/Layout';
import {UsersList} from '../../components/Admin';

class UsersDashboard extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	render() {
		return (
				<Layout title={"Users Dashboard - " + config.adminPageTitle}>
					<UsersList />
	  		</Layout>
	  	)
 	}
}

export default UsersDashboard;
