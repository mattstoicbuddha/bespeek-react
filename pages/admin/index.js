import React, { Component } from 'react';
import {connect} from "react-redux";
import Link from 'next/link';
import Router from 'next/router';
import Layout from '../../components/Layout';
import {UserInfo, AnsweredQuestions} from '../../components/Users/Profile';
import {getUserProfile, getUserProfileClearState} from "../../actions/Users/getProfile";
import cookieHelper from '../../helpers/cookies';


class showProfile extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
    	const role = cookieHelper.getRole();
		if (role !== 1) {
			if (role === 100) {
				Router.push("/users/profile");
			} else if (role === 200) {
				Router.push("/sponsors/dashboard");
			} else {
				Router.push("/");
			}
		}
		return;
	}

	render() {
    	const role = cookieHelper.getRole();
    	if (role !== 1) {
    		return (<div className='admin-blocked'>Nope</div>);
    	}
		const {result, loading, error} = this.props;
		const profile = result.profile || {};
		return (
				<Layout title={"Admin Menu"}>
					<div className={"admin-nav bg-white"}>
						<ul>
							<li>
								<Link href={{ pathname: '/admin/questions' }} prefetch><a>Edit Questions</a></Link>
							</li>
							<li>
								<Link href={{ pathname: '/admin/categories' }} prefetch><a>Edit Categories</a></Link>
							</li>
							<li>
								<Link href={{ pathname: '/admin/users' }} prefetch><a>Manage Users</a></Link>
							</li>
						</ul>
					</div>
	  		</Layout>
	  	)
 	}
}

const mapStateToProps = (state) => {
    return {
      result: state.UserProfile.result,
      loading: state.UserProfile.loading,
      error: state.UserProfile.error
    };
}

export default connect(mapStateToProps)(showProfile);
