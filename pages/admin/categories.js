import React, { Component } from 'react';
import {connect} from "react-redux";
import config from "../../env";
import Link from 'next/link';
import Layout from '../../components/Layout';
import {CategoriesList} from '../../components/Admin';

class CategoriesDashboard extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	render() {
		return (
				<Layout title={"Categories Dashboard - " + config.adminPageTitle}>
					<CategoriesList />
	  		</Layout>
	  	)
 	}
}

export default CategoriesDashboard;
