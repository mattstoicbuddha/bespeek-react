import React, { Component } from 'react';
import {connect} from "react-redux";
import config from "../../env";
import Link from 'next/link';
import Layout from '../../components/Layout';
import {QuestionsList} from '../../components/Admin';

class QuestionsDashboard extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	render() {
		return (
				<Layout title={"Questions Dashboard - " + config.adminPageTitle}>
					<QuestionsList />
	  		</Layout>
	  	)
 	}
}

export default QuestionsDashboard;
