const express = require('express')
      next = require('next'),
      dev = process.env.NODE_ENV !== 'production',
      app = next({ dev }),
      handler = app.getRequestHandler();

app.prepare()
.then(() => {
  express().use(handler).listen(3000)
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})