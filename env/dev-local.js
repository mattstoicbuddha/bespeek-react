module.exports = {
	api: "http://localhost:3500",
	adminApiPrefix: "/admin",
	adminPageTitle: "Bespeek Admin",
	image_cdn: "https://d2f9vhb426scp3.cloudfront.net",
	default_profile_image: "https://bespeek-react-assets.s3-us-west-2.amazonaws.com/usr.png"
}