let env = process.env.REACT_APP_NODE_ENV;

if (!env) {
	// `now dev` will not pull from process properly; shit is annoying 
	Object.keys(process.env).forEach(e => {
		if (e === 'REACT_APP_NODE_ENV') {
			env = process.env[e];
		}
	})
};

if (!env) env = 'dev'; // TODO: remove this and figure out why env isn't populating

const config = require("./" + env);

// Set up our root directory so we don't have to call it a bunch of times
config.cwd = process.cwd();

export default config;