const Joi = require('joi'),
	  h = require('../../helpers');

// Validate our registration form
const questionCategoriesSchema = Joi.object().keys({
    category_name: Joi.string().min(2).required(),
    category_description: Joi.string().min(2)
});

const validate = (fields) => {
	return h.checkValidation(questionCategoriesSchema, fields)
}

module.exports = {
	validate
}