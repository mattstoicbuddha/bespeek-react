const Joi = require('joi'),
	  h = require('../../helpers');

// Validate our registration form
const loginSchema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }).required()
});

const validate = (fields) => {
	return h.checkValidation(loginSchema, fields)
}

module.exports = {
	validate
}