const Joi = require('joi'),
	  h = require('../../helpers');

// Validate our registration form
const registrationSchema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(6).max(50).required(),
    first_name: Joi.string().min(3).max(30).required(),
    last_name: Joi.string().min(3).max(30).required()
});

const validate = (fields) => {
	return h.checkValidation(registrationSchema, fields)
}

module.exports = {
	validate 
}