// Helper for command line functionality
class commandHelper {
	constructor(templateDir) {
		if (!templateDir) {
			throw "No template directory passed";
		}
		this.templateDir = templateDir;
	}
	/**
	 * Create a component directory
	 * @since 1.0.0
	 *
	 * @param {object} meta - The meta we want to save for this journal
	 * @param {object} config - The configuration options for the journal type
	 * @returns {array}
	 */
}

module.exports = commandHelper;