import React, { Component } from 'react';
import Router from 'next/router';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './style.scss';

class TEMPLATE_CLASS extends Component {
  constructor(props) {
    super(props);
  }

  render() {

  }
}
const mapStateToProps = (state) => {
  return {
    result: state.TEMPLATE_CLASS.result,
    loading: state.TEMPLATE_CLASS.loading,
    error: state.TEMPLATE_CLASS.error
  };
}

export default connect(mapStateToProps)(TEMPLATE_CLASS);