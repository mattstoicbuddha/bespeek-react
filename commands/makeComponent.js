const rootdir = process.cwd(),
	  fs = require('fs'),
	  commandHelper = require('./commandHelper'),
	  templateDir = rootdir + "/commands/templates/makeComponent/";

module.exports = (name) => {
	let componentTemplate;
	try {
		// Create the directory for our component
		fs.mkdirSync(rootdir + "/components/" + name, { recursive: false });
	} catch (e) {
		console.log("Unable to create component directory: ", e.message);
		return;
	}
	// Define our component directory to put everything in
	const compDir = rootdir + "/components/" + name + "/";
	try {
		// Get our template to replace variables
		componentTemplate = fs.readFileSync(templateDir + "component.js", 'utf8');
	} catch (e) {
		console.log("Unable to read component template: ", e.message);
		return;
	}
	try {
		// Write our component
		const componentWrite = fs.writeFileSync(compDir + "index.js", componentTemplate.replace(/TEMPLATE_CLASS/g, name));
		console.log("Component index.js Created");
	} catch (e) {
		console.log("Unable to write component index.js: ", e.message);
		return;
	}
	try {
		// Write our stylesheet
		const stylesheetWrite = fs.writeFileSync(compDir + "style.scss", "");
		console.log("Stylesheet Created");
	} catch (e) {
		console.log("Unable to write component stylesheet: ", e.message);
		return;
	}

}