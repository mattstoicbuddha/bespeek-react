import Cookies from 'universal-cookie';
import {cipherTools} from './index';
const cKey = 'ae4@GUTX*Hbv=gPK*3[3kCX(b!VjBq';

const cookies = new Cookies();

const getTokenCookie = () => {
  return cookies.get("bespeek_token");
}

const setTokenCookie = (token, role) => {
  cookies.set('bespeek_token', token, { path: '/', maxAge: ( 3600 * 12 ) });
  const cipher = cipherTools.cipher(cKey);
  cookies.set('bespeek_role', cipher("role_" + role), { path: '/', maxAge: ( 3600 * 12 ) });
}

const clearTokenCookie = () => {
  cookies.set('bespeek_token', "", { path: '/', maxAge: ( 1 ) });
  cookies.set('bespeek_role', "", { path: '/', maxAge: ( 1 ) });
  cookies.set("bespeek_sponsor", "", { path: '/', maxAge: ( 1 ) });
}

const isSponsor = () => {
  return cookies.get("bespeek_sponsor") ? true : false;
}

const setSponsor = () => {
  return cookies.set("bespeek_sponsor", 1, { path: '/', maxAge: ( 3600 * 12 ) });
}

const getRole = () => {
  const role = cookies.get("bespeek_role");
  if (!role) return null;
  const decipher = cipherTools.decipher(cKey);
  const roleValue = decipher(role).replace("role_", '');
  return !isNaN(roleValue) ? Number(roleValue) : null;
}

const getAnalyticsSession = () => {
  return cookies.get("bespeek_analytics_session");
}

const setAnalyticsSession = (session) => {
  cookies.set('bespeek_analytics_session', session, { path: '/', maxAge: ( 3600 * 12 ) });
}

export default {
	getTokenCookie,
	setTokenCookie,
	clearTokenCookie,
	isSponsor,
	setSponsor,
	getRole,
  setAnalyticsSession,
  getAnalyticsSession
}