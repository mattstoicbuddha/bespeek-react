import config from '../env';
import cookieHelper from "./cookies";

const start = async () => {
  return await fetch(config.api + '/analytics/start', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(res => res.json())
      .then(async json => {
        if (!json.success) {
          return new Error(json.error);
        }
        await cookieHelper.setAnalyticsSession(json.response.session);
        return json.response.session;
      })
      .catch(error => {return new Error(error)});
}

const sendEvent = async (event, category) => {
  let session = cookieHelper.getAnalyticsSession();
  if (!session) {
    session = await start();
    console.log({session})
  }
  const body = {
    event,
    category,
    url: window.location.href,
    browser: window.navigator.userAgent,
    session
  }
  return await fetch(config.api + '/analytics/save', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(json => {
        if (!json.success) {
          return new Error(json.error);
        }
        return json.response;
      })
      .catch(error => {return new Error(error)});
}

const send = (ev, cat) => sendEvent(ev, cat).then(sent => console.log({sent})).then(err => console.log({err}));

export default {
  send
}