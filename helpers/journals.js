// Helper class specific to journal functionality
class journalHelper {
	/**
	 * Filter unnecessary values from the meta array before sending to the API
	 * @since 1.0.0
	 *
	 * @param {object} meta - The meta we want to save for this journal
	 * @param {object} config - The configuration options for the journal type
	 * @returns {array}
	 */
	filterMeta(meta, config) {
		const storage = [];
		for(let key in meta) {
			if (config.validMetaKeys().indexOf(key) > -1 && meta.hasOwnProperty(key)) {
				storage.push({
					meta_key: key,
					meta_value: meta[key]
				});
			}
		}
		return storage;
	}
}

module.exports = new journalHelper;