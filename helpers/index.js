
// Format the field names for error returns
const formatFieldNames = (field) => {
	// Break out the field name we need by splitting it from the nearby quotes
	const fieldSplit = field.split("\"");
	const fieldName = fieldSplit[1];
	fieldSplit[1] = fieldName.replace(/(^|_)./g, s => " " + s.slice(-1).toUpperCase());
	// If we have a space at the beginning of the word, which we probably will,
	// we need to remove it
	if (fieldSplit[1].charAt(0) === " ") {
		// This will only be replaced once since we aren't passing any regex
		fieldSplit[1] = fieldSplit[1].replace(" ", "");
	}
	return fieldSplit.join("\"");
}

// Run our JOI validation check and determine if we have an error or not
const checkValidation = (validateObj, fields) => {
	const isValid = validateObj.validate(fields);
	if (isValid.error) {
		// If we have more than one message, they each need to be included
		// in what we return, so we parse them out to send back
		const messages = isValid.error.details.map(function(err) {
			return formatFieldNames(err.message);
		});
		// Return our array of messages
		return messages
	}
	return null;
};

// Capitalize the first letter of each word pased
function capitalizeFirstLetter(string) {
	if (string.indexOf(" ") > -1) {
		const holder = string.split(" ").map(str => {
			return str.charAt(0).toUpperCase() + str.slice(1);
		});
		return holder.join(" ");
	} else {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
}

// Check if a function is a function
function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

function getQuestionFromList(qList, qid) {
	const question = qList.filter(q => {
		return q.id === qid;
	});
	if (question[0]) return question[0];
	return false;
}

function getCategoryFromList(cList, cid) {
	const category = cList.filter(c => {
		return c.id === cid;
	});
	if (category[0]) return category[0];
	return false;
}

const cipher = salt => {
    const textToChars = text => text.split('').map(c => c.charCodeAt(0));
    const byteHex = n => ("0" + Number(n).toString(16)).substr(-2);
    const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);

    return text => {
    	return text.toString().split('')
        .map(textToChars)
        .map(applySaltToChar)
        .map(byteHex)
        .join('');
    }
}

const decipher = salt => {
    const textToChars = text => text.split('').map(c => c.charCodeAt(0));
    const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);
    return encoded => encoded.match(/.{1,2}/g)
        .map(hex => parseInt(hex, 16))
        .map(applySaltToChar)
        .map(charCode => String.fromCharCode(charCode))
        .join('');
}

module.exports = {
	checkValidation,
	capitalizeFirstLetter,
	isFunction,
	getQuestionFromList,
	getCategoryFromList,
	cipherTools: {
		cipher,
		decipher
	}
}