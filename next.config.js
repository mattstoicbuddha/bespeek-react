const withSass = require('@zeit/next-sass');

const sass = withSass();

module.exports = {
	webpack: sass.webpack,
	env: {
		REACT_APP_NODE_ENV: process.env.REACT_APP_NODE_ENV
	},
	target: "serverless"
};
