import { combineReducers } from "redux";

import UserRegistration from "./Users/register";
import UserLogin from "./Users/login";
import UserProfile from "./Users/profile";
import UpdateUserProfile from "./Users/updateProfile";
import UserStory from "./Users/story";
import ResetPasswordGet from "./Users/resetpassword/get";
import ResetPasswordCheck from "./Users/resetpassword/check";
import ResetPasswordSave from "./Users/resetpassword/save";
import ResendConfirmation from "./Users/resendConfirmation";
import ConfirmRegistration from "./Users/confirmRegistration";
import GetQuestion from "./Questions/get";
import GetQuestionById from "./Questions/getById";
import GetAnswers from "./Answers/get";
import SaveAnswer from "./Answers/save";
import UpdateAnswer from "./Answers/update";
import {GetConnections, AddConnections, GetConnectionProfile} from "./Users/Connections";

// Sponsors
import SponsorProfile from "./Sponsors/getProfile";
import SponsorProfileUpdate from "./Sponsors/updateProfile";
import SponsorEmbedCodeSettings from "./Sponsors/getEmbedCodeSettings";
import SponsorEmbedCodeSettingsUpdate from "./Sponsors/updateEmbedCodeSettings";


// Admin Reducers
import AdminQuestionsGet from "./Admin/Questions/get";
import AdminQuestionsSave from "./Admin/Questions/save";
import AdminQuestionsCategoriesGet from "./Admin/Categories/get";
import AdminQuestionsCategoriesSave from "./Admin/Categories/save";
import AdminUsersGet from "./Admin/Users/get";
import AdminUsersReactivate from "./Admin/Users/reactivate";
import AdminUsersDeactivate from "./Admin/Users/deactivate";

export default combineReducers({
  
  AddConnections,
  GetConnections,
  GetConnectionProfile,
  UserRegistration,
  UserLogin,
  GetQuestion,
  GetQuestionById,
  GetAnswers,
  SaveAnswer,
  UpdateAnswer,
  UserProfile,
  UserStory,
  ResetPasswordGet,
  ResetPasswordCheck,
  ResetPasswordSave,
  ResendConfirmation,
  ConfirmRegistration,
  UpdateUserProfile,

  // Sponsors
  SponsorProfile,
  SponsorProfileUpdate,
  SponsorEmbedCodeSettings,
  SponsorEmbedCodeSettingsUpdate,

  // Admin Reducers
  AdminQuestionsGet,
  AdminQuestionsSave,
  AdminQuestionsCategoriesGet,
  AdminQuestionsCategoriesSave,
  AdminUsersGet,
  AdminUsersReactivate,
  AdminUsersDeactivate
});