import {
  FETCH_GET_EMBED_CODE_SETTINGS_BEGIN,
  FETCH_GET_EMBED_CODE_SETTINGS_SUCCESS,
  FETCH_GET_EMBED_CODE_SETTINGS_FAILURE,
  FETCH_GET_EMBED_CODE_SETTINGS_CLEAR_STATE
} from '../../actions/Sponsors/getEmbedCodeSettings';

const initialState = {
  result: {},
  loading: false,
  error: null
};

export default function getEmbedCodeSettingsReducer(state = initialState, action) {
  switch(action.type) {
    case FETCH_GET_EMBED_CODE_SETTINGS_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_GET_EMBED_CODE_SETTINGS_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        result: action.payload.message.response
      };

    case FETCH_GET_EMBED_CODE_SETTINGS_FAILURE:
      // The request failed, but it did stop, so set loading to "false".
      // Save the error, and we can display it somewhere
      // Since it failed, we don't have items to display anymore, so set it empty.
      // This is up to you and your app though: maybe you want to keep the items
      // around! Do whatever seems right.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        result: {}
      };

    case FETCH_GET_EMBED_CODE_SETTINGS_CLEAR_STATE:
      // We need to clear out the current state, due to leaving the login page or
      // something similar
      return {
        ...state,
        loading: false,
        error: null,
        result: {},
        token: null
      }
    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}