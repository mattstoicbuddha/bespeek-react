import {
  FETCH_QUESTION_LIST_GET_BEGIN,
  FETCH_QUESTION_LIST_GET_SUCCESS,
  FETCH_QUESTION_LIST_GET_FAILURE
} from '../../../actions/Admin/Questions/get';

const initialState = {
  result: {},
  loading: false,
  error: null
};

export default function getQuestionListReducer(state = initialState, action) {
  switch(action.type) {
    case FETCH_QUESTION_LIST_GET_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_QUESTION_LIST_GET_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        result: action.payload.message
      };

    case FETCH_QUESTION_LIST_GET_FAILURE:
      // The request failed, but it did stop, so set loading to "false".
      // Save the error, and we can display it somewhere
      // Since it failed, we don't have items to display anymore, so set it empty.
      // This is up to you and your app though: maybe you want to keep the items
      // around! Do whatever seems right.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        result: {}
      };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}